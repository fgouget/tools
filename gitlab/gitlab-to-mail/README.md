# GitLab <-> Mailing List bridge

## How to use
 1. Copy `example.cfg` and name it `projectname.cfg`.
 2. Tweak the values found inside.
 3. Run `gitlabtomail.py /path/to/projectname.cfg` to generate emails from gitlab changes.
 4. Run `mailtogitlab.py /path/to/projectname.cfg < email` to generate gitlab changes from an email.

## Suggested ways to achieve that
 * Setup ingestion IMAP mailbox that subscribes to the mailing list.
 * Setup a user with a .forward that relays emails inbound
 * Run `runner.py projectname` in a loop / as service
   - Set up a gitlab webhook to trigger http://hostname:8000/trigger
 * Setup [`getmail`](https://pyropus.ca./software/getmail/) to check that
    mailbox and feed any emails to `./mailtogitlab.py projectname` using its
    [`MDA_external` feature](https://pyropus.ca./software/getmail/configuration.html#destination-mdaexternal)
 * Run getmail in a loop similar to the one above / as a cron job.

## TODOs:
 * Make db.py into a class that allows it to be passed the database path
 * Maybe have replies from the mailing list be 'unresolved'?
 * Super difficult: have responses from the mailing list generate the diff line_range junk
