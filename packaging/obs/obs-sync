#!/bin/bash

set -e

basedir=$HOME/wine/packages/obs
gpg_user=wine-devel@winehq.org

obs_mirror=$(pwd)/mirror/download.opensuse.org/repositories/Emulators:/Wine:
winehq_dir=/home/ftp/pub/wine-builds

deb_archs="i386 amd64"
rpm_archs="i686 x86_64 src"

# compress a file while keeping the original
compress_file ()
{
    gzip -9fkn $1
    xz -fk $1
}

# download packages from OBS
download_packages ()
{
    # deb packages
    apt-mirror apt-mirror.cfg
    ./var/clean.sh
    rm -rf var

    # rpm packages
    dnf reposync --delete -c yum.conf -g
}

# add rpm files for a single Fedora release
# usage: add_rpm <distro> <obsdir>
add_rpm ()
{
    src=$obs_mirror/Fedora/$2
    dstdir=dest/fedora/$1

    echo "***** fedora $1 *****"

    test -d $dstdir || mkdir -p $dstdir
    for arch in $rpm_archs
    do
        rm -f $dstdir/$arch/$wine_pkg-$rpmver-*.$arch.rpm $dstdir/$arch/$winehq_pkg-$rpmver-*.$arch.rpm
        rsync -aq $src/$arch/$wine_pkg-$rpmver-*.$arch.rpm $dstdir/$arch/
        files=$dstdir/$arch/$wine_pkg-$rpmver-*.$arch.rpm
        if test $arch != src
        then
            files="$files $dstdir/$arch/$winehq_pkg-$rpmver-*.$arch.rpm"
            rsync -aq $src/$arch/$winehq_pkg-$rpmver-*.$arch.rpm $dstdir/$arch/
        fi
        rpm -K $files
        rpm -D "_gpg_name $gpg_user" --addsign $files
    done

    index_rpm $1
}

# add debian files for a single suite
# usage: add_deb <platform> <codename> <obsname> [<suite>]
add_deb ()
{
    obsdir=$obs_mirror/Debian/$3
    dstdir=dest/$1/dists/$2/main

    echo "***** $1 $2 *****"

    test -d $dstdir || mkdir -p $dstdir
    for arch in $deb_archs
    do
        if test "$arch" = all
        then
            rsync -aq $obsdir/$arch/${all_pkg}_$debver\~${2}-${build}_$arch.deb $dstdir/binary-$arch/
        else
            rsync -aq $obsdir/$arch/${wine_pkg}_$debver\~${2}-${build}_$arch.deb $obsdir/$arch/${winehq_pkg}_$debver\~${2}-${build}_$arch.deb $dstdir/binary-$arch/
        fi
    done
    dscverify -u $obsdir/$source_pkg\~$2-$build.dsc
    if test -f $obsdir/$source_pkg\~$2-$build.diff.gz
    then
        rsync -aq $obsdir/$source_pkg\~$2-$build.dsc $obsdir/$source_pkg\~$2-$build.diff.gz $obsdir/$source_pkg\~$2.orig.tar.[gx]z $dstdir/source/
    else
        rsync -aq $obsdir/$source_pkg\~$2-$build.dsc $obsdir/$source_pkg\~$2-$build.debian.tar.xz $obsdir/$source_pkg\~$2.orig.tar.[gx]z $dstdir/source/
    fi

    if test -n "$4"
    then
        rm -f dest/$1/dists/$4
        ln -s $2 dest/$1/dists/$4
    fi

    index_deb $1 $2 $4
}

# create an rpm repo file
# usage: make_repo <dir>
make_repo ()
{
    cat <<EOF
[WineHQ]
name=WineHQ packages
type=rpm-md
baseurl=https://dl.winehq.org/wine-builds/$1
gpgcheck=1
gpgkey=https://dl.winehq.org/wine-builds/winehq.key
enabled=1
EOF
}

# index rpm files for a single Fedora release
# usage: index_rpm <distro>
index_rpm ()
{
    dest=dest/fedora/$1

    rm -rf $dest/repodata
    createrepo_c -c $basedir/.cache -u https://dl.winehq.org/wine-builds/fedora/$1 $dest
    make_repo fedora/$1 >$dest/winehq.repo
    gpg -u $gpg_user --detach-sign --armor $dest/repodata/repomd.xml
}

# create a debian Release file
# usage: make_release <suite> <arch>
make_release ()
{
    cat <<EOF
Component: main
Origin: dl.winehq.org
Label: winehq
Description: WineHQ packages
Archive: $1
Architecture: $2
EOF
}

# create an apt .sources file
# usage: make_apt_sources <platform> <codename>
make_apt_sources ()
{
    cat <<EOF
Types: deb
URIs: https://dl.winehq.org/wine-builds/$1
Suites: $2
Components: main
Architectures: amd64 i386
Signed-By: /etc/apt/keyrings/winehq-archive.key
EOF
}

# index debian packages for a single suite
# usage: index_deb <platform> <codename> [<suite>]
index_deb ()
{
    dest=dest/$1/dists/$2
    make_apt_sources $1 $2 >$dest/winehq-$2.sources
    apt_ftparchive="apt-ftparchive -c $basedir/apt.cfg -d $basedir/.cache/apt-cache-$2.db -s $basedir/apt-override"

    for arch in $deb_archs
    do
        rm -f $dest/main/binary-$arch/Packages*
        (cd dest/$1 && $apt_ftparchive packages dists/$2/main/binary-$arch) >$dest/main/binary-$arch/Packages
        compress_file $dest/main/binary-$arch/Packages
        make_release $2 $arch >$dest/main/binary-$arch/Release
    done
    rm -f $dest/main/source/Sources*
    (cd dest/$1 && $apt_ftparchive sources dists/$2/main/source) >$dest/main/source/Sources
    compress_file $dest/main/source/Sources
    make_release $2 source >$dest/main/source/Release

    rm -f $dest/Release $dest/Release.gpg $dest/InRelease
    $apt_ftparchive -o APT::FTPArchive::Release::Codename=$2 -o APT::FTPArchive::Release::Suite=${3:-$2} release $dest >Release && mv Release $dest/Release
    gpg -u $gpg_user --clearsign -o $dest/InRelease $dest/Release
    gpg -u $gpg_user --detach-sign --armor -o $dest/Release.gpg $dest/Release
}

# publish updated files to the main ftp directory
publish_files ()
{
    key=$(cat auth_key)

    diff -ur --no-dereference $winehq_dir dest >diffs || true
    rsync -av --delete-after --delay-updates --exclude-from=rsync-filter --link-dest=$(pwd)/dest dest/ $winehq_dir

    # purge the download cache for symlinked directories
    for i in debian/dists/stable debian/dists/testing
    do
        for f in $(cd $winehq_dir && find -L $i -mtime -1 -type f)
        do
            purge_url $key dl.winehq.org /wine-builds/$f
        done
        for f in $(cd $winehq_dir && find -L $i -type d)
        do
            purge_url $key dl.winehq.org /wine-builds/$f/
        done
    done
}

cd $basedir

distros="37 38 bullseye bookworm trixie focal jammy lunar"
dry_run=
build=1

while [ $# -ge 1 ]
do
    case "$1" in
        -b)
            build="$2"
            shift
            ;;
        -d)
            distros="$2"
            shift
            ;;
        -n)
            dry_run=1
            ;;
        download)
            download_packages
            ;;
        sync)
            publish_files
            ;;
        [0-9]*)
            rpmver=${1/-rc/.rc}
            debver=${1/-rc/~rc}
            case "$2" in
                "") echo "Branch not specified for version $1"; exit 1 ;;
                vkd3d)
                    wine_pkg="vkd3d-*"
                    winehq_pkg="libvkd3d*"
                    all_pkg="libvkd3d*"
                    source_pkg="vkd3d_$debver"
                    deb_archs="$deb_archs all"
                    ;;
                staging)
                    wine_pkg="wine-staging*"
                    winehq_pkg="winehq-staging"
                    source_pkg="wine-staging_$debver"
                    ;;
                *)
                    wine_pkg="wine-$2*"
                    winehq_pkg="winehq-$2"
                    source_pkg="wine_$debver"
                    ;;
            esac
            shift

            rsync -aq --delete --exclude-from=rsync-filter --link-dest=$winehq_dir $winehq_dir/ dest
            for i in $distros
            do
                case $i in
                    3?)       add_rpm $i Fedora_$i ;;
                    bullseye) add_deb debian $i Debian_11 oldstable ;;
                    bookworm) add_deb debian $i Debian_12 stable ;;
                    trixie)   add_deb debian $i Debian_Testing_standard testing ;;
                    focal)    add_deb ubuntu $i xUbuntu_20.04 ;;
                    jammy)    add_deb ubuntu $i xUbuntu_22.04 ;;
                    lunar)    add_deb ubuntu $i xUbuntu_23.04 ;;
                    *) echo "Unknown distro $i"; exit 1 ;;
                esac
            done
            if test -z "$dry_run"
            then
                publish_files
            else
                echo "Packages updated, please run $0 sync"
            fi
            ;;
        *)
            echo "Usage: $0 download | sync | <version> {stable|devel|staging}"
            exit 1
            ;;
    esac
    shift
done
