# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Base class for list blocks
#
# Copyright 2009 Ge van Geldorp
# Copyright 2014, 2018, 2021-2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

=head1 NAME

ObjectModel::CGI::CollectionBlock - Base class for list blocks

Generates a table representing the items contained in the specified collection
and provides support for:
- Linking to a page providing a detailed view of a given item.
- Selecting individual items and performing an action on them.
  See GetItemActions() and OnItemActions().
- Performing a global action. See GetActions() and OnAction().

Note that a web page may contain multiple collection blocks.

Also, because this is only one part of a web page, some methods, such as those
dealing with errors, are delegated to the EnclosingPage object which must
implement the ObjectModel::CGI::Page interface.

=cut

package ObjectModel::CGI::CollectionBlock;

use Exporter 'import';
our @EXPORT = qw(new);

use URI::Escape;

use ObjectModel::CGI::Table;
use ObjectModel::CGI::ValueFormatter;


my $Unique;

=pod
=over 12

=item C<new()>

Creates a table showing the content of the specified Collection with optional
support for performing actions on selected rows, or performing global actions.

Parameters:
=over
=Collection
The collection of objects to show in the table.

=item EnclosingPage
The page the table will be inserted in. This page object must implement the
ObjectModel::CGI::Page interface.

=back

Note that by default the table is in read-write mode (see SetReadWrite()).

=back
=cut

sub new($$$@)
{
  my $class = shift;
  my ($Collection, $EnclosingPage) = @_;

  my $self = {Collection => $Collection,
              EnclosingPage => $EnclosingPage,
              RW => 1,
              Unique => $Unique++,
  };
  $self = bless $self, $class;
  $self->_initialize(@_);
  return $self;
}

sub _initialize($$$)
{
  #my ($self, $Collection, $EnclosingPage) = @_;
}

sub Create($$@)
{
  return ObjectModel::CGI::CollectionBlock->new(@_);
}

=pod
=over 12

=item C<SetReadWrite()>

If set to true, the table allows selecting rows and applying actions to
them (see GetItemActions()), as well as performing global actions (see
GetActions()).

Otherwise none of this is allowed, regardless of what GetItemActions() and
GetActions() return.

=back
=cut

sub SetReadWrite($$)
{
  my ($self, $RW) = @_;

  $self->{RW} = $RW;
}

sub GetDetailsPage($)
{
  my ($self) = @_;

  return $self->{Collection}->GetItemName() . "Details.pl";
}

sub escapeHTML($$)
{
  my ($self, $String) = @_;

  return $self->{EnclosingPage}->escapeHTML($String);
}


#
# Error handling framework
#

sub GenerateErrorDiv($)
{
  my ($self) = @_;

  $self->{EnclosingPage}->GenerateErrorDiv();
}

sub GenerateErrorPopup($)
{
  my ($self) = @_;

  $self->{EnclosingPage}->GenerateErrorPopup();
}


#
# Individual item property support
#


=pod
=over 12

=item C<GetPropertyDescriptors()>

Returns the list of columns for the collection table.

By default this matches the property list of the items in the collection and
that list is then pruned by DisplayProperty().

However it is possible to redefine this method to reorder the columns or add
extra columns where the values will be synthesized by GetPropertyValue() or
generated directly by GenerateDataView().

=back
=cut

sub GetPropertyDescriptors($)
{
  my ($self) = @_;
  return $self->{Collection}->GetPropertyDescriptors();
}

=pod
=over 12

=item C<DisplayProperty()>

Specifies whether to hide the given property and how to format it.

Returns a (display, format) array where the format value is optional.
If the display value is false the property is hidden. Otherwise it should be
"ro" to get a read-only view.

The format value is passed to GenerateDataView() which can use it to decide
how to format the property. By default it is simply passed to
ValueFormatter::GenerateValueHTML().

=back
=cut

sub DisplayProperty($$)
{
  my ($self, $PropertyDescriptor) = @_;

  return $PropertyDescriptor->GetClass eq "Detailref" ? "" : "ro";
}

sub GetSortedItems($$)
{
  my ($self, $Items) = @_;

  return $self->{Collection}->GetSortedItems($Items);
}

=pod
=over 12

=item C<GetDetailsLink()>

Returns the URL of the details page for the current row's object.

If the URL contains parameters those must already be encoded (see uri_escape())
but the returned URL as a whole may still need to be escaped for use in HTML
documents (notably for the ampersands, see escapeHTML()).

See GenerateDataView() for details about the Row parameter.

=back
=cut

sub GetDetailsLink($$)
{
  my ($self, $Row) = @_;

  if (!$Row->{DetailsLink})
  {
    $Row->{DetailsLink} = "$Row->{DetailsPage}?Key=". uri_escape($Row->{Item}->GetFullKey());
  }
  return $Row->{DetailsLink};
}

=pod
=over 12

=item C<GenerateHeaderView()>

Generates an HTML snippet for the column title.

The Col parameter describes the column to generate the header for.
It contains the following information:
=over 12
=item Descriptor
The corresponding PropertyDescriptor object.

=item Display
The value returned by DisplayProperty(), normally "ro".

=item Index
The index of the column (see $Row->{Cols}).

=back

The Row parameter contains the following information:
=over 12

=item PropertyDescriptors
The list of item properties, including those that DisplayProperty() says
should be hidden.

=item Cols
The collection block's list of columns. This excludes any hidden property.
The following relation holds: $Row->{Cols}->[$Col->{Index}] == $Col

=item DetailsPage
The base link to the item details page if any. See GetDetailsPage().

=item ItemActions
The list of per-item actions.

=item Row
The row index; 0 for the header.

=item Rows
The total number of rows (excluding the header).

=back

=back
=cut

sub GenerateHeaderView($$$)
{
  my ($self, $_Row, $Col) = @_;

  print $self->escapeHTML($Col->{Descriptor}->GetDisplayName());
}

=pod
=over 12

=item C<GetPropertyValue()>

Returns the underlying property value.

This is useful for providing values for the synthetic columns.
See GenerateDataView().

=back
=cut

sub GetPropertyValue($$$)
{
  my ($self, $Row, $Col) = @_;

  my $PropertyName = $Col->{Descriptor}->GetName();
  return $Row->{Item}->$PropertyName;
}

=pod
=over 12

=item C<GenerateDataView()>

Generates an HTML snippet representing the property value in a user-readable
form.

See GenerateHeaderView() for a description of the Row and Col parameters.
The Row parameter has the following extra fields:
=over 12

=item Row
The row number starting at 1.

=item Item
The object for the current row.

=back

Furthermore one can get the link to the current object's details page by
passing the row to GetDetailsLink().

The Col parameter has the following extra field:
=over 12
=item Format
The format to be used to display the property.
If undefined the default formatting is used.

=back

=back
=cut

sub GenerateDataView($$$)
{
  my ($self, $Row, $Col) = @_;

  my $Value = $self->GetPropertyValue($Row, $Col);
  GenerateValueHTML($self, $Col->{Descriptor}, $Value, $Col->{Format});
}


#
# Collection table generation
#

sub GenerateFormStart($)
{
  my ($self) = @_;

  print "<form action='" . $ENV{"SCRIPT_NAME"} . "' method='post'>\n";
  my ($MasterColNames, $MasterColValues) = $self->{Collection}->GetMasterCols();
  if (defined($MasterColNames))
  {
    foreach my $ColIndex (0..$#{$MasterColNames})
    {
      print "<div><input type='hidden' name='", $MasterColNames->[$ColIndex],
            "' value='", $self->escapeHTML($MasterColValues->[$ColIndex]),
            "' /></div>\n";
    }
  }
}

sub GenerateHeaderCell($$$)
{
  my ($self, $Row, $Col) = @_;

  print "<th>";
  $self->GenerateHeaderView($Row, $Col);
  print "</th>\n";
}

sub GenerateHeaderRow($$)
{
  my ($self, $Row) = @_;

  print "<tr>\n";
  if (@{$Row->{ItemActions}} and $Row->{Rows})
  {
    $self->{EnclosingPage}->GenerateImportJS(GetTableJSFile());
    print "<th>";
    GenerateMasterCheckbox("block$self->{Unique}");
    print "</th>\n";
  }
  foreach my $Col (@{$Row->{Cols}})
  {
    $self->GenerateHeaderCell($Row, $Col);
  }
  print "</tr>\n";
}

sub SelName($$)
{
  my ($self, $Key) = @_;

  $Key =~ s/[^0-9a-zA-Z]+/_/g;
  return "sel_" . $Key;
}

sub GenerateDataCell($$$)
{
  my ($self, $Row, $Col) = @_;

  print "<td>";
  my $CloseLink;
  if ($Row->{DetailsPage} and $Col->{Descriptor}->GetIsKey())
  {
    print "<a href='", $self->escapeHTML($self->GetDetailsLink($Row)), "'>";
    $CloseLink = "</a>";
  }
  $self->GenerateDataView($Row, $Col);
  print "$CloseLink</td>\n";
}

sub GenerateDataRow($$)
{
  my ($self, $Row) = @_;

  my $Class = ($Row->{Row} % 2) == 0 ? "even" : "odd";
  print "<tr class='$Class'>\n";
  if (@{$Row->{ItemActions}})
  {
    print "<td><input name='", $self->SelName($Row->{Item}->GetKey()),
          "' type='checkbox' cbgroup='block$self->{Unique}'/></td>\n";
  }
  foreach my $Col (@{$Row->{Cols}})
  {
    $self->GenerateDataCell($Row, $Col);
  }
  print "</tr>\n";
}

sub GenerateFormEnd($)
{
  #my ($self) = @_;
  print "</form>\n";
}

sub GenerateList($)
{
  my ($self) = @_;

  my $Collection = $self->{Collection};
  my $PropertyDescriptors = $self->GetPropertyDescriptors();

  my $ColIndex = 0;
  my (@Cols, $HasDT);
  foreach my $PropertyDescriptor (@$PropertyDescriptors)
  {
    my ($Display, $Format) = $self->DisplayProperty($PropertyDescriptor);
    next if (!$Display);
    push @Cols, {Descriptor => $PropertyDescriptor,
                 Display => $Display,
                 Format => $Format,
                 Index => $ColIndex++};
    $HasDT ||= ($PropertyDescriptor->GetClass() eq "Basic" and
                $PropertyDescriptor->GetType() eq "DT");
  }
  my $Items = $self->{Collection}->GetSortedItems();
  if ($HasDT and @$Items != 0)
  {
    $self->{EnclosingPage}->GenerateImportJS(GetDateTimeJSFile());
  }

  print "<div class='CollectionBlock'>\n";
  $self->GenerateFormStart();
  $self->GenerateErrorDiv();

  my $Items = $self->{Collection}->GetSortedItems();
  print "<table border='0' cellpadding='5' cellspacing='0' summary='" .
        "Overview of " . $Collection->GetCollectionName() . "'>\n";
  print "<thead>\n";
  my $Row = {
    PropertyDescriptors => $PropertyDescriptors,
    Cols => \@Cols,
    DetailsPage => $self->GetDetailsPage(),
    ItemActions => $self->{RW} ? $self->GetItemActions() : [],
    Row => 0, # 0 for the header ---> 1 for the first line
    Rows => scalar(@$Items),
  };
  $self->GenerateHeaderRow($Row);
  print "</thead>\n";

  print "<tbody>\n";
  foreach my $Item (@$Items)
  {
    $Row->{Row}++;
    $Row->{Item} = $Item;
    $Row->{DetailsLink} = undef;
    $self->GenerateDataRow($Row);
  }
  if (@$Items == 0)
  {
    print "<tr class='even'><td colspan='0'>No entries</td></tr>\n";
  }

  print "</tbody>\n";
  print "</table>\n";

  if (@{$Row->{ItemActions}} and @$Items)
  {
    print "<div class='CollectionBlockActions'>For selected ", $self->{Collection}->GetCollectionName() . ":";
    foreach my $Action (@{$Row->{ItemActions}})
    {
      print " <input type='submit' name='Action' value='" .
            $self->escapeHTML($Action) . "' />";
    }
    print "\n";
    print "</div>\n";
  }

  my $Actions = $self->{RW} ? $self->GetActions() : [];
  if (@$Actions != 0)
  {
    print "<div class='CollectionBlockActions'>\n";
    foreach my $Action (@$Actions)
    {
      print "<input type='submit' name='Action' value='$Action' />\n";
    }
    print "</div>\n";
  }

  $self->GenerateErrorPopup(undef);
  $self->GenerateFormEnd();
  print "</div>\n";
}


#
# Per-item actions handling
#

=pod
=over 12

=item C<GetItemActions()>

Returns the list of per-item actions.

This is a set of actions that can be applied to each selected item in the
collection.

If the set is empty the GUI will provide no way to select items. Otherwise a
column of checkboxes is added.

=back
=cut

sub GetItemActions($)
{
  #my ($self) = @_;

  return ["Delete"];
}

=pod
=over 12

=item C<OnItemAction()>

This method is called to apply $Action to each selected item.

Note that it is the responsibility of this method to validate the property
values by calling Validate() before performing the action.

Returns true on success. In case an error occurs while performing the action on
any of the selected items, the enclosing page's error message is set and
false is returned.

=back
=cut

sub OnItemAction($$$)
{
  my ($self, $Item, $Action) = @_;

  if ($self->{RW} and $Action eq "Delete")
  {
    return $self->{EnclosingPage}->AddError($self->{Collection}->DeleteItem($Item)) ? 0 : 1;
  }

  $self->{EnclosingPage}->AddError("No per-Item action defined for $Action");
  return 0;
}


#
# Actions handling
#

sub GetAddButtonLabel($)
{
  my ($self) = @_;

  my $Label = $self->{Collection}->GetItemName();
  $Label =~ s/([a-z])([A-Z])/$1 $2/g;
  return "Add $Label";
}

=pod
=over 12

=item C<GetActions()>

Returns the list of global actions.

This is a set of actions that do not impact any specific item in the
collection.

=back
=cut

sub GetActions($)
{
  my ($self) = @_;

  return $self->GetDetailsPage() ? [$self->GetAddButtonLabel()] : [];
}

=pod
=over 12

=item C<OnAction()>

Implements the "Add" and per-item actions.

Note that it is the responsibility of this method to validate the property
values by calling Validate() before performing the action.

Returns true on success. Otherwise the enclosing page's error message is set
and false is returned.

=back
=cut

# Note that there is no call to OnAction() in the CollectionBlock class so
# there is no need for a CallOnAction() trampoline.
sub OnAction($$)
{
  my ($self, $Action) = @_;

  if ($self->{RW} and $Action eq $self->GetAddButtonLabel())
  {
    exit($self->{EnclosingPage}->Redirect($self->GetDetailsPage()));
  }

  foreach my $Key (@{$self->{Collection}->GetKeys()})
  {
    if (defined $self->{EnclosingPage}->GetParam($self->SelName($Key)))
    {
      my $Item = $self->{Collection}->GetItem($Key);
      return 0 if (!$self->OnItemAction($Item, $Action));
    }
  }
  return 1;
}

1;
