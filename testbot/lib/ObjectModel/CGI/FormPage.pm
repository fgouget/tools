# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Base class for web pages containing a form
#
# Copyright 2009 Ge van Geldorp
# Copyright 2012, 2014, 2017-2018, 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package ObjectModel::CGI::FormPage;

=head1 NAME

ObjectModel::CGI::FormPage - Base class for web forms

This class generates a page that:
- Displays a list of properties, allows editing them, or a mix of both.
- Provides a list of actions that can be applied after visualizing or editing
  the properties.
- Provides the framework for validating the property values and saving them.

This page has the following properties:
=over

=item Method
Specifies how to send the CGI parameters for the property values (get or post).

=item HasRequired
True if the form has fields that are mandatory.

=item ActionPerformed
True if an action was specified and performed.

=back

A typical use takes this form:

my $PropertyDescriptors = [ ... ];
my $MyFormPage = MyFormPage->new($Request, "required-privileges", $PropertyDescriptors);
$MyFormPage->GeneratePage();

=cut

use ObjectModel::CGI::Page;
our @ISA = qw(ObjectModel::CGI::Page);

use ObjectModel::CGI::ValueFormatter;


=pod
=over 12

=item C<_initialize()>

This method is called by the constructor to initialize the form page.

The PropertyDescriptors parameter specifies a reference to the list of
properties that the form should show or allow editing, as specified by
DisplayProperty(). It may be undefined if determining the property list is
delegated to GetPropertyDescriptors().

By default the form allows modifying the field values.

See also Page::new().

=back
=cut

sub _initialize($$$$)
{
  my ($self, $Request, $RequiredRole, $PropertyDescriptors) = @_;

  $self->SUPER::_initialize($Request, $RequiredRole);

  $self->{PropertyDescriptors} = $PropertyDescriptors;
  $self->{RW} = 1;
  $self->{HasRequired} = !1;
  $self->{ActionPerformed} = !1;
  $self->{Method} = "post";
  $self->{HasRW} = !1;
}

=pod
=over 12

=item C<SetReadWrite()>

If set to true, the form allows editing the fields.
Otherwise it only shows their values.

=back
=cut

sub SetReadWrite($$)
{
  my ($self, $RW) = @_;

  $self->{RW} = $RW;
}


#
# Property handling
#

=pod
=over 12

=item C<GetPropertyDescriptors()>

Returns a reference to a list of ObjectModel::PropertyDescriptor objects
describing the fields to be displayed / edited. This also defines the set of
CGI parameters that the page supports.

This must not return undef.

=back
=cut

sub GetPropertyDescriptors($)
{
  my ($self) = @_;

  return $self->{PropertyDescriptors};
}

sub GetPropertyDescriptorByName($$)
{
  my ($self, $Name) = @_;

  foreach my $PropertyDescriptor (@{$self->GetPropertyDescriptors()})
  {
    if ($PropertyDescriptor->GetName() eq $Name)
    {
      return $PropertyDescriptor;
    }
  }

  return undef;
}

=pod
=over 12

=item C<DisplayProperty()>

Specifies whether to hide, display or allow editing the given property, and
optionally how to format it.

Returns a (display, format) array where the display value is one of the
following:
=over
=item ""
Any value evaluating to false specifies that the property should be hidden.

=item "ro"
Specifies that the property is read-only, i.e. GenerateField() should display
it using the default format instead of generating an edit field.

=item "rw"
Specifies that GenerateField() should allow editing the property.
Note that this implies that $PropertyDescriptor->GetName() should be a valid
HTTP parameter name.

=back

The optional format value is passed to GenerateValueInput() and
GenerateValueView() which can use it to decide how to present the value.
In the default implementation the format is only used for read-only properties
and is passed to ValueFormatter::GenerateValueHTML().

=back
=cut

sub DisplayProperty($$)
{
  my ($self, $PropertyDescriptor) = @_;

  return # Detailref fields point to a Collection of objects matching the
         # primary key of this form data. As such they can neither be shown
         # nor edited.
         $PropertyDescriptor->GetClass() eq "Detailref" ? "" :
         # Itemref fields are keys identifying other objects and thus would
         # require careful validation if edited. Requiring users to manually
         # type the appropriate key value would also be cumbersome so this is
         # currently not supported. But they can be displayed.
         $PropertyDescriptor->GetClass() eq "Itemref" ? "ro" :
         $PropertyDescriptor->GetIsReadOnly() ? "ro" :
         # All other properties can be displayed,
         !$self->{RW} ? "ro" :
         # and even edited if in read-write mode...
         $PropertyDescriptor->GetClass() ne "Basic" ? "rw" :
         $PropertyDescriptor->GetType() ne "S" ? "rw" :
         # ...except autoincrement ones
         "ro";
}

=pod
=over 12

=item C<GetFirstErrField()>

Returns the first invalid field in the form order.

See Page::GetFirstErrField().

=back
=cut

sub GetFirstErrField($)
{
  my ($self) = @_;

  if (defined $self->GetErrMessage())
  {
    foreach my $PropertyDescriptor (@{$self->GetPropertyDescriptors()})
    {
      if ($self->IsErrField($PropertyDescriptor->GetName()))
      {
        return $PropertyDescriptor->GetName();
      }
    }
  }
  return undef;
}

=pod
=over 12

=item C<GetPropertyValue()>

Returns the underlying property value.

This is used to initialize the form with the properties of the 'object' to be
modified or displayed. The default implementation considers that there is no
underlying value.

=back
=cut

sub GetPropertyValue($$)
{
  #my ($self, $PropertyDescriptor) = @_;
  return undef;
}


#
# Form field support
#

=pod
=over 12

=item C<GetDisplayName()>

Returns the unescaped property's label / description / user-friendly name.
Escaping the name for HTML is the responsibility of the caller.

=back
=cut

sub GetDisplayName($$)
{
  my ($self, $PropertyDescriptor) = @_;

  return $PropertyDescriptor->GetDisplayName();
}

=pod
=over 12

=item C<GetFormValue()>

Returns the value to be used for this form's property.

This is either the value received through the HTTP parameters, or if it was
not specified there, the underlying property value (see GetPropertyValue()).

Converting that value into a form suitable for viewing (including escaping it
for HTML) or editing is the responsibility of GenerateValueView() and
GenerateValueInput() respectively.

=back
=cut

sub GetFormValue($$)
{
  my ($self, $PropertyDescriptor) = @_;

  my $Value = $self->GetParam($PropertyDescriptor->GetName());
  return defined $Value ? $Value :
         $self->GetPropertyValue($PropertyDescriptor);
}

=pod
=over 12

=item C<GenerateValueView()>

Generates an HTML snippet representing the value in a user-readable form.

=back
=cut

sub GenerateValueView($$$$)
{
  my ($self, $PropertyDescriptor, $Value, $Format) = @_;

  GenerateValueHTML($self, $PropertyDescriptor, $Value, $Format);
}

sub GetInputType($$)
{
  my ($self, $PropertyDescriptor) = @_;

  # Note: Editing Detailrefs and Itemrefs is not supported which leaves only
  # Basic and Enum property descriptors here.
  return $PropertyDescriptor->GetClass() eq "Enum" ? "select" :
         $PropertyDescriptor->GetType() eq "B" ? "checkbox" :
         $PropertyDescriptor->GetType() eq "textarea" ? "textarea" :
         "text";
}

=pod
=over 12

=item C<GenerateRequired()>

Generates an HTML snippet to be put next to an input field to indicate it must
be set.

=back
=cut

sub GenerateRequired($$)
{
  my ($self, $PropertyDescriptor) = @_;

  if ($PropertyDescriptor->GetIsRequired())
  {
    $self->{HasRequired} = 1;
    print "&nbsp;<a class='Required' title='Required field'>*</a>";
  }
}

=pod
=over 12

=item C<GenerateValueInput()>

Generates an HTML snippet allowing the specified value to be edited.

=back
=cut

sub GenerateValueInput($$$$)
{
  my ($self, $PropertyDescriptor, $Value, $_Format) = @_;

  my $InputType = $self->GetInputType($PropertyDescriptor);
  if ($InputType eq "checkbox")
  {
    my $Checked = $Value ? " checked" : "";
    print "<input type='checkbox' name='", $PropertyDescriptor->GetName(),
          "'$Checked/>";
  }
  elsif ($InputType eq "select")
  {
    print "<select name='", $PropertyDescriptor->GetName(), "'>\n";
    foreach my $V (@{$PropertyDescriptor->GetValues()})
    {
      print "  <option value='", $self->escapeHTML($V), "'";
      print " selected" if ($Value eq $V);
      print ">", $self->escapeHTML($V), "</option>\n";
    }
    print "</select>";
    # No need to add a 'required' label since checkboxes always have a value
  }
  elsif ($InputType eq "textarea")
  {
    my $MaxLength = $PropertyDescriptor->GetMaxLength();
    print "<textarea name='", $PropertyDescriptor->GetName(), "' cols='";
    if ($MaxLength < 50)
    {
      print "$MaxLength' rows='1'>";
    }
    else
    {
      print "50' rows='", int(($MaxLength + 49) / 50),  "'>";
    }
    print $self->escapeHTML($Value) if (defined $Value);
    print "</textarea>";
    $self->GenerateRequired($PropertyDescriptor);
  }
  else
  {
    my $Size=$PropertyDescriptor->GetMaxLength();
    $Size = 50 if ($Size > 50);
    print "<input type='$InputType' name='", $PropertyDescriptor->GetName(),
          "' maxlength='", $PropertyDescriptor->GetMaxLength(),
          "' size='$Size'";
    if (defined $Value and $InputType ne "password")
    {
      print " value='", $self->escapeHTML($Value), "'";
    }
    print "/>";
    $self->GenerateRequired($PropertyDescriptor);
  }
}


#
# HTML form page generation
#

sub GetTitle($)
{
  #my ($self) = @_;
  return undef;
}

sub GenerateTitle($)
{
  my ($self) = @_;

  my $Title = $self->GetTitle();
  if ($Title)
  {
    print "<h1 id='PageTitle'>", $self->escapeHTML($Title), "</h1>\n";
  }
}

sub GetHeaderText($)
{
  #my ($self) = @_;
  return undef;
}

sub GenerateFormStart($)
{
  my ($self) = @_;
  print "<form action='" . $ENV{"SCRIPT_NAME"} .
       "' method='$self->{Method}' enctype='multipart/form-data'>\n";
}

sub GenerateField($$$$)
{
  my ($self, $PropertyDescriptor, $Display, $Format) = @_;

  my $Class = $self->IsErrField($PropertyDescriptor->GetName()) ?
              " class='errorfield'" : "";
  print "<div class='ItemProperty'><label$Class>",
        $self->escapeHTML($self->GetDisplayName($PropertyDescriptor)),
        "</label>";

  my $Value = $self->GetFormValue($PropertyDescriptor);
  if ($Display eq "rw")
  {
    $self->{HasRW} = 1;
    print "<div class='ItemValue'>";
    $self->GenerateValueInput($PropertyDescriptor, $Value, $Format);
    print "</div>";
  }
  else
  {
    $self->GenerateValueView($PropertyDescriptor, $Value, $Format);
  }

  print "</div>\n";
}

sub GenerateFields($)
{
  my ($self) = @_;

  foreach my $PropertyDescriptor (@{$self->GetPropertyDescriptors()})
  {
    my ($Display, $Format) = $self->DisplayProperty($PropertyDescriptor);
    $self->GenerateField($PropertyDescriptor, $Display, $Format) if ($Display);
  }
}

sub GenerateRequiredLegend($)
{
  my ($self) = @_;

  if ($self->{HasRequired})
  {
    print "<div class='ItemProperty'><label><span class='Required'>*</span></label>Required field</div>\n";
  }
}

sub GenerateFormEnd($)
{
  #my ($self) = @_;
  print "</form>\n";
}

sub GetFooterText($)
{
  #my ($self) = @_;
  return undef;
}

sub GenerateBody($)
{
  my ($self) = @_;

  print "<div class='ItemBody'>\n";
  $self->GenerateTitle();
  print "<div class='Content'>\n";
  my $Text = $self->GetHeaderText();
  if ($Text)
  {
    print "<p>$Text</p>\n";
  }
  $self->GenerateFormStart();
  $self->GenerateErrorDiv();

  $self->GenerateFields();
  $self->GenerateRequiredLegend();

  if (defined $self->GetErrMessage())
  {
    foreach my $ErrField (sort $self->GetErrFields())
    {
      my $PropertyDescriptor = $self->GetPropertyDescriptorByName($ErrField);
      if ($PropertyDescriptor and !($self->DisplayProperty($PropertyDescriptor))[0])
      {
        # The hidden fields should not be invalid
        $self->AddError("The hidden $ErrField field is invalid (internal error?)", $ErrField);
      }
    }
    $self->GenerateErrorPopup();
  }
  $self->GenerateActions();
  $self->GenerateFormEnd();
  $Text = $self->GetFooterText();
  if ($Text)
  {
    print "<p>$Text</p>\n";
  }
  print "</div><!--Content-->\n";
  print "</div><!--ItemBody-->\n";
}

sub GeneratePage($)
{
  my ($self) = @_;

  if ($self->GetParam("Action"))
  {
    $self->{ActionPerformed} = $self->OnAction($self->GetParam("Action"));
  }

  $self->SUPER::GeneratePage();
}


#
# Validating and saving the form content
#

=pod
=over 12

=item C<Validate()>

Validates the property values.

Note that this method is not called in the default implementation. Subclasses
would likely want to call it in OnAction() or possibly in _initialize() after
having set up the property descriptors list.

The default implementation only checks the individual property values using
the corresponding property descriptor's ValidateValue() method.

Any cross-property consistency checks should be done by redefining this method
in subclasses.

=back
=cut

sub Validate($)
{
  my ($self) = @_;

  my $Ret = 1;
  foreach my $PropertyDescriptor (@{$self->GetPropertyDescriptors()})
  {
    my $Value = $self->GetParam($PropertyDescriptor->GetName());
    my $ErrMessage = $PropertyDescriptor->ValidateValue($Value, 1);
    if ($ErrMessage)
    {
      $self->AddError($ErrMessage, $PropertyDescriptor->GetName());
      $Ret = !1;
    }
  }

  return $Ret;
}

sub SaveProperty($$$)
{
  #my ($self, $PropertyDescriptor, $Value) = @_;
  die "Pure virtual function FormPage::SaveProperty called";
}

sub Save($)
{
  my ($self) = @_;

  my @ParamNames = $self->GetParamNames();
  foreach my $ParameterName (@ParamNames)
  {
    my $PropertyDescriptor = $self->GetPropertyDescriptorByName($ParameterName);
    if (defined($PropertyDescriptor))
    {
      if (! $self->SaveProperty($PropertyDescriptor, $self->GetParam($ParameterName)))
      {
        return !1;
      }
    }
  }

  foreach my $PropertyDescriptor (@{$self->GetPropertyDescriptors()})
  {
    if ($PropertyDescriptor->GetClass() eq "Basic" &&
        $PropertyDescriptor->GetType() eq "B" &&
        ($self->DisplayProperty($PropertyDescriptor))[0] eq "rw" &&
        ! defined($self->GetParam($PropertyDescriptor->GetName())))
    {
      if (! $self->SaveProperty($PropertyDescriptor, ""))
      {
        return !1;
      }
    }
  }
  return 1;
}


#
# Actions handling
#

=pod
=over 12

=item C<GetActions()>

Returns a list of actions that can be applied after completing or reading this
form. Each action is a string which is used as the label of a button at the
bottom of the form.

If an action name starts with a dot, the button label omits the dot and the
corresponding <input> field gets no name attribute. Note that this means the
action name is not sent to the form so it probably does not make sense to have
more than one such action per form.

By default there is no action.

=back
=cut

sub GetActions($)
{
  #my ($self) = @_;
  return [];
}

sub GenerateActions($)
{
  my ($self) = @_;

  print "<div class='ItemActions'>\n";
  foreach my $Action (@{$self->GetActions()})
  {
    print "<input type='submit' ";
    print "name='Action' " if ($Action !~ s/^\.//);
    print "value='$Action'/>\n";
  }
  print "</div>\n";
}

=pod
=over 12

=item C<OnAction()>

This method should be redefined in subclasses to implement the supported
actions.

Note that it is the responsibility of this method to validate the property
values by calling Validate() before performing the action.

Returns true on success. Otherwise sets the page's error message and returns
false.

=back
=cut

sub OnAction($$)
{
  my ($self, $Action) = @_;

  $self->AddError("No action defined for $Action");
  return 0;
}

1;
