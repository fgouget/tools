# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Copyright 2018-2020 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package WineTestBot::LogUtils;

=head1 NAME

WineTestBot::LogUtils - Provides functions to parse task logs

=cut


use Exporter 'import';
our @EXPORT = qw(GetLogFileNames GetLogLabel
                 GetLogLineCategory GetReportLineCategory
                 ParseTaskLog ParseWineTestReport
                 SnapshotLatestReport UpdateLatestReport UpdateLatestReports
                 CreateLogErrorsCache LoadLogErrorsFromFh LoadLogErrors);

use Algorithm::Diff;
use File::Basename;

use ObjectModel::Collection; # For CombineKey()
use WineTestBot::Config; # For $MaxUnitSize
use WineTestBot::Failures;
use WineTestBot::Tasks; # FIXME Hack for SaveLogFailures()
use WineTestBot::Utils; # For LocaleName()


#
# Task log parser
#

=pod
=over 12

=item C<_IsPerlError()>

Returns true if the string looks like a Perl error message.

=back
=cut

sub _IsPerlError($)
{
  my ($Str) = @_;

  return $Str =~ /^Use of uninitialized value / ||
         $Str =~ /^Undefined subroutine / ||
         $Str =~ /^Global symbol / ||
         $Str =~ /^Possible precedence issue /;
}

=pod
=over 12

=item C<GetLogLineCategory()>

Identifies the category of the given log line: an error message, a Wine
diagnostic line, a TestBot error, etc.

The category can then be used to decide whether to hide the line or, on
the contrary, highlight it.

=back
=cut

sub GetLogLineCategory($)
{
  my ($Line) = @_;

  if (# Build messages
      $Line =~ /^\+ \S/ or
      $Line =~ /^LANG=/ or
      $Line =~ /^Running (?:the tests|WineTest) / or
      $Line =~ /^Task: (?:ok|tests|Updated)/ or
      # testbot.log information messages
      $Line =~ /^----- Run / or
      $Line =~ /:info:/)
  {
    return "info";
  }
  if (# Build messages
      $Line =~ /^warning:/ or
      $Line =~ /: warning:/)
  {
    return "warning";
  }
  if (# Git errors
      $Line =~ /^CONFLICT / or
      $Line =~ /^error: patch failed:/ or
      $Line =~ /^error: corrupt patch / or
      # Build errors
      # Note that just ': error:' leads to false positives. So assume the
      # error location (source filename + line number) contains no space.
      $Line =~ /^[^ ]+: fatal error: / or # nonexistent include file
      $Line =~ /^[^ ]+: error: / or
      $Line =~ /: undefined reference to `/ or
      $Line =~ /^make: [*]{3} No rule to make target / or
      $Line =~ /^Makefile:\d+: recipe for target .* failed$/ or
      $Line =~ /^Task: / or
      # Typical perl errors
      _IsPerlError($Line) or
      # The TestLauncher / testbot.log errors
      $Line =~ /:error: / or
      # WineTest.exe errors
      $Line =~ /^Error: /)
  {
    return "error";
  }
  if ($Line =~ /:winediag:/)
  {
    if (# Normally CorIsLatestSvc() would not be called but mscoree:mscoree
        # calls it as a test.
        $Line =~ /CorIsLatestSvc If this function is called,/ or
        # VMs and most test machines don't have a midi port.
        $Line =~ /No software synthesizer midi port found,/ or
        # Most VMs have limited OpenGL support.
        $Line =~ /None of the requested D3D feature levels is supported / or
        # The tests are not run as root.
        $Line =~ /this requires special permissions/)
    {
      return "diag";
    }
    return "error";
  }
  if ($Line =~ /^[0-9a-f]+:err:ntdll:RtlpWaitForCriticalSection/)
  {
    # The tests should not have "wait timed out in thread" critical section
    # errors.
    return "error";
  }
  if (# TestBot script error messages
      $Line =~ /^[a-zA-Z.]+:error: / or
      # TestBot error
      $Line =~ /^BotError:/ or
      $Line =~ /^Error:/ or
      # X errors
      $Line =~ /^X Error of failed request: / or
      $Line =~ / opcode of failed request: /)
  {
    return "boterror";
  }

  return "none";
}


sub _AddLogGroup($$;$)
{
  my ($LogInfo, $GroupName, $LineNo) = @_;

  # In theory the error group names are all unique. But, just in case, make
  # sure we don't overwrite $LogInfo->{MsgGroups}->{$GroupName}.
  if (!$LogInfo->{MsgGroups}->{$GroupName})
  {
    push @{$LogInfo->{MsgGroupNames}}, $GroupName;
    $LogInfo->{MsgGroups}->{$GroupName} = {
      LineNo => $LineNo || 0,
      LineNos => [],
      Types => [],
      Messages => [],
    };
  }
  return $LogInfo->{MsgGroups}->{$GroupName};
}

sub _SetLogMsgIsNew($$$;$$)
{
  my ($LogInfo, $MsgGroup, $MsgIndex, $IsNew) = @_;

  if (!!$MsgGroup->{IsNew}->[$MsgIndex] ne !!$IsNew)
  {
    $MsgGroup->{IsNew}->[$MsgIndex] = $IsNew;
    my $Type = $MsgGroup->{Types}->[$MsgIndex];
    my $Delta = $IsNew ? 1 : -1;
    $MsgGroup->{"${Type}New"} += $Delta;
    $LogInfo->{"${Type}New"} += $Delta;
  }
}

sub _AddLogMsg($$$;$$)
{
  my ($LogInfo, $MsgGroup, $Line, $LineNo, $TypeAndNew) = @_;
  my $Type = lc($TypeAndNew);

  push @{$MsgGroup->{Messages}}, $Line;
  push @{$MsgGroup->{LineNos}}, $LineNo || 0;
  push @{$MsgGroup->{Types}}, $Type;
  $MsgGroup->{"${Type}Count"}++;
  $LogInfo->{"${Type}Count"}++;
  if ($Type ne $TypeAndNew)
  {
    _SetLogMsgIsNew($LogInfo, $MsgGroup, $#{$MsgGroup->{Messages}}, 1);
  }
}


=pod
=over 12

=item C<ParseTaskLog()>

Returns a hashtable containing a summary of the task log:
=over

=item LogName
The log file basename.

=item LogPath
The full log file path.

=item Type
'tests' if the task ran Wine tests and 'build' otherwise.

=item Task
Either 'ok' if the task was successful or a code indicating why it failed.

=item TestLauncher.exe...
Entries named after the binaries and files of interest to the TestBot that
have been updated.

=item BadLog
Contains an error message if the task log could not be read.

=item eCount, eNew
Respectively the total number of type 'e' (error) messages,
and the count of those that are new.

=item MsgGroupNames
An array containing the names of all the message groups.

=item MsgGroups
A hashtable indexed by the message group name. Each entry contains:

=over

=item LineNo
The line number of the start of this message group. Note that this is normally
different from the line of the first message in that group.

=item eCount, eNew
Respectively the total number of type 'e' (error) messages in this group,
and the count of those that are new.

=item Types
An array containing the message types. Currently there is only 'e', for errors.

=item Messages
An array containing the messages.

=item LineNos
An array containing the line number of the message in the log file.

=back

=back
=back
=cut

sub ParseTaskLog($)
{
  my ($LogPath) = @_;

  my $LogName = basename($LogPath);
  my $LogInfo = {
    LogName => $LogName,
    LogPath => $LogPath,

    MsgGroupNames => [],
    MsgGroups => {},
  };

  my $LogFile;
  if (!open($LogFile, "<", $LogPath))
  {
    $LogInfo->{BadLog} = "Unable to open '$LogName' for reading: $!";
    return $LogInfo;
  }

  my $CurGroup;
  my $LineNo = 0;
  $LogInfo->{Type} = "build";
  foreach my $Line (<$LogFile>)
  {
    $LineNo++;
    chomp $Line;
    $Line =~ s/\r+$//;

    my $Category = GetLogLineCategory($Line);
    if ($Category eq "error")
    {
      $CurGroup = _AddLogGroup($LogInfo, "", $LineNo) if (!$CurGroup);
      _AddLogMsg($LogInfo, $CurGroup, $Line, $LineNo, "e");
    }
    elsif ($Category eq "warning")
    {
      $CurGroup = _AddLogGroup($LogInfo, "", $LineNo) if (!$CurGroup);
      _AddLogMsg($LogInfo, $CurGroup, $Line, $LineNo, "w");
    }

    if ($Line eq "Task: tests")
    {
      $LogInfo->{Type} = "tests";
    }
    elsif ($Line eq "Task: ok")
    {
      $LogInfo->{Task} ||= "ok";
    }
    elsif ($Line eq "Task: Patch failed to apply")
    {
      $LogInfo->{Task} = "badpatch";
      last; # Should be the last and most specific message
    }
    elsif ($Line =~ /^Task: Updated ([a-zA-Z0-9.]+)$/)
    {
      $LogInfo->{$1} = "updated";
    }
    elsif ($Line =~ /^Task: / or _IsPerlError($Line))
    {
      $LogInfo->{Task} = "failed";
    }
  }
  close($LogFile);
  $LogInfo->{Task} ||= "missing";
  return $LogInfo;
}


#
# WineTest report parser
#

=pod
=over 12

=item C<GetReportLineCategory()>

Identifies the category of the given test report line: an error message,
a todo, just an informational message or none of these.

The category can then be used to decide whether to hide the line or, on
the contrary, highlight it.

=back
=cut

sub GetReportLineCategory($)
{
  my ($Line) = @_;

  if ($Line =~ /:[0-9]* Test marked todo: /)
  {
    return "todo";
  }
  if ($Line =~ /:[0-9.]* Tests skipped: / or
      $Line =~ /^[_.a-z0-9-]+:[_a-z0-9]* skipped / or
      $Line =~ /^[_.a-z0-9-]+:\d+:[0-9.]* Line has been silenced after \d+ occurrences$/ or
      $Line =~ /^[0-9a-f]{4}:[_a-z0-9]+:[0-9.]* Silenced \d+ todos, \d+ skips and \d+ traces\.$/)
  {
    return "skip";
  }
  if ($Line =~ /^    \w+=dll is (?:missing|a stub)/ or
      $Line =~ /^    \w+=(?:version error \d+|version not (?:found|present))/ or
      $Line =~ /^[_.a-z0-9-]+:[_a-z0-9]* start / or
      $Line =~ /: this is the last test seen before the exception/)
  {
    return "info";
  }

  return "none";
}


sub _NewCurrentUnit($$)
{
  my ($Dll, $Unit) = @_;

  return {
    # There is more than one test unit when running the full test suite so keep
    # track of the current one. Note that for the TestBot we don't count or
    # complain about misplaced skips.
    Dll => $Dll,
    Unit => $Unit,
    Units => {$Unit => 1},
    UnitsRE => $Unit,
    UnitSize => 0,
    LineFailures => 0,
    LineFlaky => 0,
    LineTodos => 0,
    LineSkips => 0,
    SummaryFailures => 0,
    SummaryFlaky => 0,
    SummaryTodos => 0,
    SummarySkips => 0,
    IsBroken => 0,
    Rc => undef,
    Pids => {},
    Group => undef,
    GroupLineNo => 0,
  };
}

sub _AddExtra($$;$)
{
  my ($LogInfo, $Error, $Cur) = @_;

  if (defined $Cur)
  {
    $Error = "$Cur->{Dll}:$Cur->{Unit} $Error";
    $Cur->{Extra}++;
  }
  push @{$LogInfo->{Extra}}, $Error;
}

sub _CheckUnit($$$$)
{
  my ($LogInfo, $Cur, $Unit, $Type) = @_;

  # _CheckUnit() is only called for Wine test lines.
  $LogInfo->{IsWineTest} = 1;

  # Only report the first misplaced message to avoid duplicate errors.
  if (!$Cur->{Units}->{$Unit} and $Cur->{Unit} ne "" and !$Cur->{IsBroken})
  {
    _AddExtra($LogInfo, "contains a misplaced $Type line for $Unit", $Cur);
    $Cur->{IsBroken} = 1;
  }
}

sub _CheckSummaryCounter($$$$)
{
  my ($LogInfo, $Cur, $Field, $Type) = @_;

  if ($Cur->{"Line$Field"} != 0 and $Cur->{"Summary$Field"} == 0)
  {
    _AddExtra($LogInfo, "has unaccounted for $Type messages", $Cur);
  }
  elsif ($Cur->{"Line$Field"} == 0 and $Cur->{"Summary$Field"} != 0)
  {
    _AddExtra($LogInfo, "is missing some $Type messages", $Cur);
  }
}

sub _CloseTestUnit($$$)
{
  my ($LogInfo, $Cur, $Last) = @_;

  # Verify the summary lines
  if ($Cur->{Dll} ne "" and !$Cur->{IsBroken})
  {
    _CheckSummaryCounter($LogInfo, $Cur, "Failures", "failure");
    _CheckSummaryCounter($LogInfo, $Cur, "Flaky", "flaky");
    _CheckSummaryCounter($LogInfo, $Cur, "Todos", "todo");
    _CheckSummaryCounter($LogInfo, $Cur, "Skips", "skip");
  }

  # Note that the summary lines may count some failures twice
  # so only use them as a fallback.
  $Cur->{LineFailures} ||= $Cur->{SummaryFailures};

  if ($Cur->{Dll} ne "")
  {
    if ($Cur->{UnitSize} > $MaxUnitSize)
    {
      _AddExtra($LogInfo, "prints too much data ($Cur->{UnitSize} bytes)", $Cur);
    }
    if (!$Cur->{IsBroken} and defined $Cur->{Rc})
    {
      # Check the exit code, particularly against failures reported
      # after the 'done' line (e.g. by subprocesses).
      if ($Cur->{LineFailures} != 0 and $Cur->{Rc} == 0)
      {
        _AddExtra($LogInfo, "returned success despite having failures", $Cur);
      }
      elsif (!$LogInfo->{IsWineTest} and $Cur->{Rc} != 0)
      {
        _AddExtra($LogInfo, "The test returned a non-zero exit code");
      }
      elsif ($LogInfo->{IsWineTest} and $Cur->{LineFailures} == 0 and
             $Cur->{Rc} != 0)
      {
        _AddExtra($LogInfo, "returned a non-zero exit code despite reporting no failures", $Cur);
      }
    }
    # For executables TestLauncher's done line may not be recognizable.
    elsif ($LogInfo->{IsWineTest} and !defined $Cur->{Rc})
    {
      if (!$Last)
      {
        _AddExtra($LogInfo, "has no done line (or it is garbled)", $Cur);
      }
      elsif ($Last and !$LogInfo->{TaskTimedOut})
      {
        _AddExtra($LogInfo, "The report seems to have been truncated");
      }
      elsif ($Last and $LogInfo->{TaskTimedOut})
      {
        _AddExtra($LogInfo, "timeout", $Cur);
      }
    }
  }
  if ($Cur->{Dll} and
      ($Cur->{Rc} or $Cur->{Extra} or $Cur->{LineFailures} or
       $Cur->{SummaryFailures} or $Cur->{LineFlaky} or $Cur->{SummaryFlaky}))
  {
    $LogInfo->{_FailedTestUnits}->{"$Cur->{Dll}:$Cur->{Unit}"} = 1;
  }
}

sub _AddReportMsg($$$$$)
{
  my ($LogInfo, $Cur, $LineNo, $Line, $Type) = @_;

  # Make the timeout messages more user-friendly
  my $ErrLine = $Line;
  if ($ErrLine =~ /^[^:]+:([^:]*):[0-9a-f]{4} done \(258\)/)
  {
    my $Unit = $1;
    $ErrLine = $Unit ne "" ? "$Unit: Timeout" : "Timeout";
  }

  if (!$Cur->{Group})
  {
    $Cur->{Group} = _AddLogGroup($LogInfo, $Cur->{Dll}, $Cur->{GroupLineNo} || $LineNo);
  }
  _AddLogMsg($LogInfo, $Cur->{Group}, $ErrLine, $LineNo, $Type);
}

=pod
=over 12

=item C<ParseWineTestReport()>

Returns a hashtable containing a summary of the WineTest report:
=over

=item IsWineTest
True if this is a regular Wine test report, false if this is some other Windows
binary.

=item TaskTimedOut
True if the overall task timed out waiting for the test to complete.

=item TestUnitCount
The number of test units.

=item TimeoutCount
The number of test units that timed out.

=item FailedTestUnits
A list of test units that have failures (flaky or not).

=item eCount, eNew, MsgGroupNames, MsgGroups, BadLog
See ParseTaskLog() for details.

=back
=back
=cut

sub ParseWineTestReport($$$)
{
  my ($LogPath, $IsWineTest, $TaskTimedOut) = @_;

  my $LogName = basename($LogPath);
  my $LogInfo = {
    LogName => $LogName,
    LogPath => $LogPath,
    IsWineTest => $IsWineTest,
    TaskTimedOut => $TaskTimedOut,

    TestUnitCount => 0,
    TimeoutCount => 0,
    FailedTestUnits => [],

    MsgGroupNames => [],
    MsgGroups => {},

    Extra => [],
  };

  my $LogFile;
  if (!open($LogFile, "<", $LogPath))
  {
    $LogInfo->{BadLog} = "Unable to open '$LogName' for reading: $!";
    return $LogInfo;
  }

  my $LineNo = 0;
  my $InDlls = 0;
  my $Cur = _NewCurrentUnit("", "");
  foreach my $Line (<$LogFile>)
  {
    $LineNo++;
    $Cur->{UnitSize} += length($Line);
    next if ($Line =~ /^\s*$/); # empty lines have no impact
    chomp $Line;
    $Line =~ s/\r+$//;

    if ($InDlls == 0 and $Line =~ /^Dll info:$/)
    {
      $InDlls = 1;
    }
    elsif ($InDlls == 1 and
           $Line =~ /^\s+\S+=(?:dll is native|load error \d+)$/)
    {
      _AddReportMsg($LogInfo, $Cur, $LineNo, $Line, "e");
      $Cur->{LineFailures}++;
    }
    elsif ($Line =~ /^Test output:$/)
    {
      $InDlls = 2;
    }
    elsif ($Line =~ m%^([_.a-z0-9-]+):([_a-z0-9]*) (start|skipped) (?:-|[/_.a-z0-9-]+)%)
    {
      my ($Dll, $Unit, $Type) = ($1, $2, $3);

      # Close the previous test unit
      _CloseTestUnit($LogInfo, $Cur, 0);
      $Cur = _NewCurrentUnit($Dll, $Unit);
      $LogInfo->{TestUnitCount}++;

      # Recognize skipped messages in case we need to skip tests in the VMs
      $Cur->{Rc} = 0 if ($Type eq "skipped");

      # The next error will be in a new error group
      $Cur->{GroupLineNo} = $LineNo;
      $Cur->{Group} = undef;
    }
    elsif (($Cur->{Unit} ne "" and
            $Line =~ /($Cur->{UnitsRE})\.c:\d+:[0-9.]* Subtest ([_a-z0-9]+)/) or
           $Line =~ /^([_a-z0-9]+)\.c:\d+:[0-9.]* Subtest ([_a-z0-9]+)/)
    {
      my ($Unit, $SubUnit) = ($1, $2);
      _CheckUnit($LogInfo, $Cur, $Unit, "$SubUnit subtest");
      $Cur->{Units}->{$SubUnit} = 1;
      $Cur->{UnitsRE} = join("|", keys %{$Cur->{Units}});
    }
    elsif (($Cur->{Unit} ne "" and
            $Line =~ /($Cur->{UnitsRE})\.c:\d+:[0-9.]* Test (?:failed|succeeded inside todo block): /) or
           $Line =~ /^([_a-z0-9]+)\.c:\d+:[0-9.]* Test (?:failed|succeeded inside todo block): /)
    {
      _CheckUnit($LogInfo, $Cur, $1, "failure");
      _AddReportMsg($LogInfo, $Cur, $LineNo, "$&$'", "e");
      $Cur->{LineFailures}++;
    }
    elsif (($Cur->{Unit} ne "" and
            $Line =~ /($Cur->{UnitsRE})\.c:\d+:[0-9.]* Test (?:marked flaky|succeeded inside flaky todo block): /) or
           $Line =~ /^([_a-z0-9]+)\.c:\d+:[0-9.]* Test (?:marked flaky|succeeded inside flaky todo block): /)
    {
      _CheckUnit($LogInfo, $Cur, $1, "flaky");
      _AddReportMsg($LogInfo, $Cur, $LineNo, "$&$'", "k");
      $Cur->{LineFlaky}++;
    }
    elsif (($Cur->{Unit} ne "" and
            $Line =~ /($Cur->{UnitsRE})\.c:\d+:[0-9.]* Test marked todo: /) or
           $Line =~ /^([_a-z0-9]+)\.c:\d+:[0-9.]* Test marked todo: /)
    {
      _CheckUnit($LogInfo, $Cur, $1, "todo");
      $Cur->{LineTodos}++;
    }
    elsif (($Cur->{Unit} ne "" and
            $Line =~ /($Cur->{UnitsRE})\.c:\d+:[0-9.]* Tests skipped: /) or
           $Line =~ /^([_a-z0-9]+)\.c:\d+:[0-9.]* Tests skipped: /)
    {
      my $Unit = $1;
      # Don't complain and don't count misplaced skips. Only complain if they
      # are misreported (see _CloseTestUnit).
      if ($Cur->{Units}->{$Unit} or $Cur->{Unit} eq "")
      {
        $Cur->{LineSkips}++;
      }
    }
    elsif ($Line =~ /^Fatal: test '([_a-z0-9]+)' does not exist/)
    {
      # This also replaces a test summary line.
      $Cur->{Pids}->{0} = 1;
      $Cur->{SummaryFailures}++;
      $LogInfo->{IsWineTest} = 1;

      _AddReportMsg($LogInfo, $Cur, $LineNo, $Line, "e");
      $Cur->{LineFailures}++;
    }
    elsif (($Cur->{Unit} ne "" and
            $Line =~ /($Cur->{UnitsRE})\.c:\d+:[0-9.]* IgnoreExceptions=([01])/) or
           $Line =~ /^([_.a-z0-9]+)\.c:\d+:[0-9.]* IgnoreExceptions=([01])/)
    {
      my ($Unit, $Ignore) = ($1, $2);
      _CheckUnit($LogInfo, $Cur, $Unit, "ignore exceptions");
      $Cur->{IgnoreExceptions} = $Ignore;
    }
    elsif ($Line =~ /([0-9a-f]{4}):([_.a-z0-9]+):[0-9.]* unhandled exception [0-9a-fA-F]{8} at /)
    {
      my ($Pid, $Unit) = ($1, $2);
      _CheckUnit($LogInfo, $Cur, $Unit, "unhandled exception");
      if (!$Cur->{IgnoreExceptions})
      {
        if ($Cur->{Units}->{$Unit})
        {
          # This also replaces a test summary line.
          $Cur->{Pids}->{$Pid || 0} = 1;
          $Cur->{SummaryFailures}++;
        }
        _AddReportMsg($LogInfo, $Cur, $LineNo, "$&$'", "e");
        $Cur->{LineFailures}++;
      }
    }
    elsif ($Line =~ /Unhandled exception: .* in .* code /)
    {
      if (!$Cur->{IgnoreExceptions})
      {
        # This also replaces a test summary line. The pid is unknown so use 0.
        $Cur->{Pids}->{0} = 1;
        $Cur->{SummaryFailures}++;
        _AddReportMsg($LogInfo, $Cur, $LineNo, "$&$'", "e");
        $Cur->{LineFailures}++;
      }
    }
    elsif (($Cur->{Unit} ne "" and
            $Line =~ /($Cur->{UnitsRE})\.c:\d+:[0-9.]* unhandled exception [0-9a-fA-F]{8} in child process ([0-9a-f]{4})/) or
           $Line =~ /^([_.a-z0-9]+)\.c:\d+:[0-9.]* unhandled exception [0-9a-fA-F]{8} in child process ([0-9a-f]{4})/)
    {
      my ($Unit, $Pid) = ($1, $2);
      _CheckUnit($LogInfo, $Cur, $Unit, "child exception");
      if (!$Cur->{IgnoreExceptions})
      {
        if ($Cur->{Units}->{$Unit})
        {
          # This also replaces a test summary line.
          $Cur->{Pids}->{$Pid || 0} = 1;
          $Cur->{SummaryFailures}++;
        }
        _AddReportMsg($LogInfo, $Cur, $LineNo, $&, "e");
        $Cur->{LineFailures}++;
      }
    }
    elsif ($Line =~ /([0-9a-f]{4}):([_a-z0-9]+):[0-9.]* \d+ tests? executed \((\d+) marked as todo, (?:(\d+) as flaky, )?(\d+) failures?\), (\d+) skipped\./)
    {
      my ($Pid, $Unit, $Todos, $Flaky, $Failures, $Skips) = ($1, $2, $3, $4, $5, $6);

      # Dlls that have only one test unit will run it even if there is
      # no argument.
      if ($Unit eq $Cur->{Unit} or $Cur->{Unit} eq "")
      {
        # There may be more than one summary line due to child processes
        $Cur->{Pids}->{$Pid || 0} = 1;
        $Cur->{SummaryFailures} += $Failures;
        $Cur->{SummaryFlaky} += ($Flaky || 0);
        $Cur->{SummaryTodos} += $Todos;
        $Cur->{SummarySkips} += $Skips;
        $LogInfo->{IsWineTest} = 1;
      }
      else
      {
        _CheckUnit($LogInfo, $Cur, $Unit, "test summary") if ($Todos or $Failures);
      }
    }
    elsif (($Cur->{Dll} ne "" and
            $Line =~ /(\Q$Cur->{Dll}\E):([_a-z0-9]*):([0-9a-f]{4}) done \((-?\d+)\) in /) or
           $Line =~ /^([_.a-z0-9-]+):([_a-z0-9]*):([0-9a-f]{4}) done \((-?\d+)\) in /)
    {
      my ($Dll, $Unit, $Pid, $Rc) = ($1, $2, $3, $4);

      if ($LogInfo->{IsWineTest} and ($Dll ne $Cur->{Dll} or $Unit ne $Cur->{Unit}))
      {
        # First close the current test unit taking into account
        # it may have been polluted by the new one.
        $Cur->{IsBroken} = 1;
        _CloseTestUnit($LogInfo, $Cur, 0);

        # Then switch to the new test unit, not for the past lines, but for
        # those before the next 'start' line. This 'new' test unit may have
        # inconsistent results too so set IsBroken.
        $Cur = _NewCurrentUnit($Dll, $Unit);
        $Cur->{IsBroken} = 1;

        # Finally, warn about the missing start line.
        _AddExtra($LogInfo, "had no start line (or it is garbled)", $Cur);
      }

      if ($Rc == 258)
      {
        # The done line will already be shown as a timeout (see JobDetails)
        # so record the failure but don't add an error message.
        $Cur->{IsBroken} = 1;
        $LogInfo->{TimeoutCount}++;
        _AddReportMsg($LogInfo, $Cur, $LineNo, $&, "e");
      }
      elsif ((!$Pid and !%{$Cur->{Pids}}) or
             ($Pid and !$Cur->{Pids}->{$Pid} and !$Cur->{Pids}->{0}))
      {
        # The main summary line is missing
        if ($Rc & 0xc0000000)
        {
          _AddExtra($LogInfo, sprintf("crashed (%08x)", $Rc & 0xffffffff), $Cur);
          $Cur->{IsBroken} = 1;
        }
        elsif ($LogInfo->{IsWineTest} and !$Cur->{IsBroken})
        {
          _AddExtra($LogInfo, "has no test summary line (early exit of the main process?)", $Cur);
        }
      }
      elsif ($Rc & 0xc0000000)
      {
        # We know the crash happened in the main process which means we got
        # an "unhandled exception" message. So there is no need to add an
        # extra message or to increment the failure count. Still note that
        # there may be inconsistencies (e.g. unreported todos or skips).
        $Cur->{IsBroken} = 1;
      }
      $Cur->{Rc} = $Rc;
    }
  }
  $Cur->{IsBroken} = 1 if ($LogInfo->{TaskTimedOut});
  _CloseTestUnit($LogInfo, $Cur, 1);
  close($LogFile);
  $LogInfo->{FailedTestUnits} = [ sort keys %{$LogInfo->{_FailedTestUnits}} ];

  # Move the extra errors into their own error group
  my $ExtraCount = @{$LogInfo->{Extra}};
  if ($ExtraCount)
  {
    my $ExtraGroup = _AddLogGroup($LogInfo, "Report validation errors");
    $ExtraGroup->{Messages} = $LogInfo->{Extra};
    $ExtraGroup->{LineNos} = [ (0) x $ExtraCount ];
    $ExtraGroup->{Types} = [ ("e") x $ExtraCount ];
    $ExtraGroup->{eCount} = $ExtraCount;
    $LogInfo->{eCount} += $ExtraCount;
  }

  return $LogInfo;
}


#
# Log querying and formatting
#

=pod
=over 12

=item C<GetLogFileNames()>

Scans the directory for test reports and task logs and returns their filenames.
The filenames are returned in the order in which the logs are meant to be
presented.

=back
=cut

sub GetLogFileNames($;$)
{
  my ($Dir, $IncludeOld) = @_;

  my @Globs = qw(exe32.report exe32_*.report
                 exe64.report exe64_*.report
                 win32.report win32_*.report
                 wow32.report wow32_*.report
                 wow64.report wow64_*.report
                 task.log testbot.log);
  push @Globs, ("old_task.log", "old_testbot.log") if ($IncludeOld);

  my @Logs;
  foreach my $Glob (@Globs)
  {
    foreach my $FileName (glob("'$Dir/$Glob'"))
    {
      my $LogName = basename($FileName);
      if ($LogName =~ /^([a-zA-Z0-9_]+\.report)$/)
      {
        $LogName = $1; # untaint
      }
      elsif ($LogName eq $Glob) # (old_) task.log and testbot.log cases
      {
        $LogName = $Glob; # untaint
      }
      else
      {
        # Not a valid log filename (where does this file come from?)
        next;
      }

      push @Logs, $LogName if (-f "$Dir/$LogName" and !-z _);
    }
  }
  return \@Logs;
}

my %_LogFileLabels = (
  "exe32.report"    => '32 bit%s report',
  "exe64.report"    => '64 bit%s report',
  "win32.report"    => '32 bit%s report',
  "wow32.report"    => '32 bit%s WoW report',
  "wow64.report"    => '64 bit%s WoW report',
  "task.log"        => 'task%s log',
  "testbot.log"     => 'testbot%s log',
  "old_task.log"    => 'old%s task logs',
  "old_testbot.log" => 'old%s testbot logs',
);

=pod
=over 12

=item C<GetLogLabel()>

Returns a user-friendly description of the content of the specified log file.

=back
=cut

sub GetLogLabel($)
{
  my ($LogFileName) = @_;

  my $Extra = "";
  if ($LogFileName =~ /^([^_]+)_(.*)\.report$/)
  {
    $LogFileName = "$1.report";
    $Extra = " ". LocaleName($2);
  }
  my $Label = $_LogFileLabels{$LogFileName};
  return defined $Label ? sprintf($Label, $Extra) : $LogFileName;
}


#
# Log errors caching [Part 1]
#

=pod
=over 12

=item C<LoadLogErrorsFromFh()>

Loads the specified log errors file, returning the errors in the same format
as TagNewMessages().

All lines are of the following form:
  <type> <value1> <value2>

The values depend on the <type> of the line. <type> and <value1> must not
contain spaces while <value2> runs to the end of the line.
More specifically the line formats are:
=over

=item p <name> <value>
Property lines contain (name, value) pairs.
Note that properties which can be calculated while reading the errors file
are not saved (e.g. eCount and eNew).

=item a <name> <value>
Array lines contain (name, value) pairs and can appear multiple times for a
given name, resulting in a corresponding list of values.

=item g <lineno> <groupname>
Group lines contain the group name and the line number of the first line of
the group in the log. Note that the first line would typically not be an
error line.
A group line should be followed by the old and new error lines in this group.

=item o <lineno> <line>
Old error lines contain the line number of the error in the log and a verbatim
copy of that line (with CR/LF converted to a simple LF).

=item n <lineno> <line>
The format for new error lines is identical to that for old errors but with a
different type.

=item f <errindex> <failureid1>...
Failure lines contain the index of the error in the error group and a list of
the known failures they match (normally at most one).

=back
=back
=cut

sub LoadLogErrorsFromFh($$)
{
  my ($LogInfo, $ErrorsFile) = @_;

  $LogInfo->{MsgGroupNames} ||= [];
  $LogInfo->{MsgGroups} ||= {};

  while (my $Line = <$ErrorsFile>)
  {
    $LogInfo->{LineNo}++;
    chomp $Line;

    my ($Type, $Property, $Value) = split / /, $Line, 3;
    if (!defined $Value)
    {
      $LogInfo->{BadLog} = "$LogInfo->{LineNo}: Found an invalid line";
      return $Line;
    }
    # else $Type, $Property and $Value are all defined
    elsif ($Type eq "p")
    {
      if (!defined $LogInfo->{$Property})
      {
        $LogInfo->{$Property} = $Value;
      }
      else
      {
        $LogInfo->{BadLog} = "$LogInfo->{LineNo}: Cannot set $Property = $Value because it is already set to $LogInfo->{$Property}";
        return $Line;
      }
    }
    elsif ($Type eq "a")
    {
      if (!defined $LogInfo->{$Property})
      {
        $LogInfo->{$Property} = [ $Value ];
      }
      elsif (ref($LogInfo->{$Property}) eq "ARRAY")
      {
        push @{$LogInfo->{$Property}}, $Value;
      }
      else
      {
        $LogInfo->{BadLog} = "$LogInfo->{LineNo}: $Property is not an array, its value is $LogInfo->{$Property}";
        return $Line;
      }
    }
    elsif ($Type eq "g")
    {
      $LogInfo->{CurGroup} = _AddLogGroup($LogInfo, $Value, $Property);
    }
    elsif (!$LogInfo->{CurGroup})
    {
      $LogInfo->{BadLog} = "$LogInfo->{LineNo}: Got a $Type line with no group";
      return $Line;
    }
    elsif ($Type eq "e" or $Type eq "E" or $Type eq "k" or $Type eq "K" or
           $Type eq "w" or $Type eq "W")
    {
      _AddLogMsg($LogInfo, $LogInfo->{CurGroup}, $Value, $Property, $Type);
    }
    elsif ($Type eq "o") # for backward compatibility
    {
      _AddLogMsg($LogInfo, $LogInfo->{CurGroup}, $Value, $Property, "e");
    }
    elsif ($Type eq "n") # for backward compatibility
    {
      _AddLogMsg($LogInfo, $LogInfo->{CurGroup}, $Value, $Property, "E");
    }
    elsif ($Type eq "f")
    {
      my $MsgIndex = $Property;
      my $CurGroup = $LogInfo->{CurGroup};
      $CurGroup->{Failures}->{$Property} = [ split / /, $Value ];
      if ($CurGroup->{IsNew}->[$MsgIndex])
      {
        my $Type = $CurGroup->{Types}->[$MsgIndex];
        $CurGroup->{"${Type}KnownNew"}++;
        $LogInfo->{"${Type}KnownNew"}++;
      }
    }
    else
    {
      $LogInfo->{BadLog} = "$LogInfo->{LineNo}: Found an unknown line type ($Type)";
      return $Line;
    }
  }

  return undef;
}

=pod
=over 12

=item C<LoadLogErrors()>

Loads the specified log errors file.

See _LoadLogErrorsFromFh() for the format of the errors file.

Returns the errors in the same format as TagNewMessages().

=back
=cut

sub LoadLogErrors($)
{
  my ($LogPath) = @_;

  my $LogInfo = {
    LogName => basename($LogPath),
    LogPath => $LogPath,
    MsgGroupNames => [],
    MsgGroups => {},
  };
  if (open(my $ErrorsFile, "<", "$LogPath.errors"))
  {
    LoadLogErrorsFromFh($LogInfo, $ErrorsFile);
    if ($LogInfo->{BadLog})
    {
      $LogInfo->{BadLog} = "$LogInfo->{LogName}.errors: $LogInfo->{BadLog}";
    }
    delete $LogInfo->{CurGroup};
    close($ErrorsFile);
  }
  else
  {
    $LogInfo->{BadLog} = "Unable to open '$LogInfo->{LogName}.errors' for reading: $!";
  }

  return $LogInfo;
}

=pod
=over 12

=item C<_WriteLogErrorsToFh()>

Writes the LogInfo structure in text form to the specified file descriptor.
See _LoadLogErrors() for the format of the errors file.

=back
=cut

sub _WriteLogErrorsToFh($$)
{
  my ($Fh, $LogInfo) = @_;

  # Save some properties...
  foreach my $Name ("BadRef", "NoRef")
  {
    next if (!defined $LogInfo->{$Name});
    print $Fh "p $Name $LogInfo->{$Name}\n";
  }

  # some arrays...
  foreach my $Name ("FailedTestUnits")
  {
    next if (!defined $LogInfo->{$Name});
    foreach my $Value (@{$LogInfo->{$Name}})
    {
      print $Fh "a $Name $Value\n";
    }
  }

  # and the message groups + known failures
  foreach my $GroupName (@{$LogInfo->{MsgGroupNames}})
  {
    my $Group = $LogInfo->{MsgGroups}->{$GroupName};
    print $Fh "g $Group->{LineNo} $GroupName\n";
    foreach my $MsgIndex (0..$#{$Group->{Messages}})
    {
      my $LineNo = $Group->{LineNos}->[$MsgIndex];
      my $TypeAndNew = $Group->{Types}->[$MsgIndex];
      $TypeAndNew = uc($TypeAndNew) if ($Group->{IsNew}->[$MsgIndex]);
      print $Fh "$TypeAndNew $LineNo $Group->{Messages}->[$MsgIndex]\n";

      my $Failures = $Group->{Failures}->{$MsgIndex};
      print $Fh "f $MsgIndex @$Failures\n" if ($Failures);
    }
  }
}

sub _SaveLogErrors($)
{
  my ($LogInfo) = @_;

  my $ErrorsPath = "$LogInfo->{LogPath}.errors";
  if (open(my $ErrorsFile, ">", $ErrorsPath))
  {
    _WriteLogErrorsToFh($ErrorsFile, $LogInfo);
    close($ErrorsFile);

    # Set the mtime so Janitor reaps both at the same time
    utime time(), GetMTime($LogInfo->{LogPath}), $ErrorsPath;
    return undef;
  }
  return "Could not open '$LogInfo->{LogName}.errors' for writing: $!";
}

sub _DumpErrors
{
  my ($Label, $LogInfo) = @_;

  print STDERR "$Label:\n";
  my @SubKeys;
  foreach my $Key (sort keys %{$LogInfo})
  {
    next if ($Key =~ /^(?:MsgGroupNames|MsgGroups|eNew)$/);
    if (ref($LogInfo->{$Key}) eq "HASH")
    {
      push @SubKeys, $Key;
      next;
    }

    if (ref($LogInfo->{$Key}) eq "ARRAY")
    {
      print STDERR "+ $Key\n";
      map { print STDERR "  | $_\n" } (@{$LogInfo->{$Key}});
    }
    else
    {
      print STDERR "+ $Key $LogInfo->{$Key}\n";
    }
  }
  _WriteLogErrorsToFh(*STDERR, $LogInfo);
  map { _DumpErrors("$Label.$_", $LogInfo->{$_}) } (@SubKeys);
}


#
# New error detection
#

sub _DumpDiff($$)
{
  my ($Label, $Diff) = @_;

  print STDERR "$Label:\n";
  $Diff = $Diff->Copy();
  my $Chunk = 0;
  while ($Diff->Next())
  {
    if ($Diff->Same())
    {
      print STDERR "$Chunk  $_\n" for ($Diff->Same());
    }
    else
    {
      print STDERR "$Chunk -$_\n" for ($Diff->Items(1));
      print STDERR "$Chunk +$_\n" for ($Diff->Items(2));
    }
    $Chunk++;
  }
}


=pod
=over 12

=item C<MarkAllMessagesAsNew()>

Marks all the messages as new.

The $LogInfo structure is augmented with the following fields:
=over

=item New
The total number of new messages of all types.

=item MsgGroups
=over

=item New
The number of new messages of all types in this group.

=back

=back
=back
=cut

sub MarkAllMessagesAsNew($)
{
  my ($LogInfo) = @_;

  foreach my $MsgGroup (values %{$LogInfo->{MsgGroups}})
  {
    foreach my $MsgIndex (0..$#{$MsgGroup->{Messages}})
    {
      _SetLogMsgIsNew($LogInfo, $MsgGroup, $MsgIndex, 1);
    }
    $MsgGroup->{New} = @{$MsgGroup->{Messages}};
    $LogInfo->{New} += $MsgGroup->{New};
  }
}

=pod
=over 12

=item C<_GetLineKey()>

This is a helper for _DeduplicateLatestReport() and TagNewMessages(). It
reformats the log lines so they can meaningfully be compared to the reference
log even if line numbers change, etc.

=back
=cut

sub _GetLineKey($)
{
  my ($Line) = @_;
  return undef if (!defined $Line);

  # Remove end-of-line spaces
  $Line =~ s/ +$//;

  # Remove the line number
  $Line =~ s/^([_a-z0-9]+\.c:)\d+:[0-9.]*( Test (?:failed|marked flaky|succeeded inside (?:flaky )?todo block):)/$1$2/
  or $Line =~ s/^([._a-zA-Z0-9-]+:)\d+(: recipe for target )/$1$2/

  # Remove the crash code address: it changes whenever the test is recompiled
  or $Line =~ s/^(Unhandled exception: .* code) \(0x[0-9a-fA-F]{8,16}\)\.$/$1/
  # or the process id in Wine's exc_filter() lines
  or $Line =~ s/[0-9a-f]{4}:([_a-z0-9]+:)[0-9.]*( unhandled exception [0-9a-fA-F]{8} at )[0-9a-fA-F]{8,16}$/$1$2/
  # or child process id
  or $Line =~ s/^([_a-z0-9]+\.c:)\d+:[0-9.]*( unhandled exception [0-9a-fA-F]{8} in child process )[0-9a-f]{4}/$1$2/

  # The exact amount of data printed does not change the error
  or $Line =~ s/^([_.a-z0-9-]+:[_a-z0-9]* prints too much data )\(\d+ bytes\)$/$1/

  # Remove the process id from log warnings and errors
  or $Line =~ s/^[0-9a-f]{4}:(err|fixme):/$1:/;

  # Note: The 'done (258)' lines are modified by ParseWineTestReport() and
  #       no longer contain the pid. So they need no further change here.

  return $Line;
}

=pod
=over 12

=item C<TagNewMessages()>

Compares the specified errors to the reference report to identify new errors.

The $LogInfo structure is augmented with the following fields:
=over

=item BadRef
Contains an error message if the reference log could not be read.

=item NoRef
True if there was no usable reference log. This could either mean that there
was no reference log or that the reference log could not be read in which case
BadRef would be set.

=item eNew
The total number of new type 'e' (error) messages.

=item MsgGroups
=over

=item eNew
The number of new type 'e' (error) messages in this group.

=item IsNew
An array indicating which entries in the group's Messages array are new.

=back

=back
=back
=cut

sub TagNewMessages($$)
{
  my ($LogInfo, $Task) = @_;

  return if (!@{$LogInfo->{MsgGroupNames}});

  # Then iterate over the reference logs
  my ($HasRef, $MarkedAsNew);
  my $RefReportPaths = $Task->GetRefReportPaths($LogInfo->{LogName});
 reflog: foreach my $RefReportPath (values %$RefReportPaths)
  {
    my $RefInfo = LoadLogErrors($RefReportPath);
    if (defined $RefInfo->{BadLog})
    {
      # Only save the first BadLog error
      $LogInfo->{BadRef} ||= $RefInfo->{BadLog};
      next;
    }
    $HasRef = 1;
    next if (!@{$RefInfo->{MsgGroupNames}});

    # Initially mark all errors as new if and only if we found a non-empty
    # reference report (see check above).
    if (!$MarkedAsNew)
    {
      MarkAllMessagesAsNew($LogInfo);
      $MarkedAsNew = 1;
    }

    # And unmark any error already present in the reference log
    foreach my $GroupName (@{$LogInfo->{MsgGroupNames}})
    {
      my $Group = $LogInfo->{MsgGroups}->{$GroupName};
      next if (!$Group->{New});

      my $RefGroup = $RefInfo->{MsgGroups}->{$GroupName};
      next if (!$RefGroup);

      my $Diff = Algorithm::Diff->new($RefGroup->{Messages}, $Group->{Messages},
                                      { keyGen => \&_GetLineKey });
      #_DumpDiff($GroupName, $Diff);
      my $MsgIndex = 0;
      while ($Diff->Next())
      {
        my $SameCount = $Diff->Same();
        if ($SameCount)
        {
          # Mark identical lines as not new
          while ($SameCount--)
          {
            if ($Group->{IsNew}->[$MsgIndex])
            {
              _SetLogMsgIsNew($LogInfo, $Group, $MsgIndex, 0);
              $Group->{New}--;
              $LogInfo->{New}--;
              last if (!$Group->{New});
            }
            $MsgIndex++;
          }
        }
        else
        {
          # Found zero or more new lines, skip over them
          $MsgIndex += $Diff->Items(2);
        }
      }

      # Stop here if no group has new errors anymore
      last reflog if (!$LogInfo->{New});
    }
  }
  if (!$HasRef)
  {
    # Do not tag the errors as new: this is up to the caller.
    $LogInfo->{NoRef} = 1;
  }
}

=pod
=over 12

=item C<MatchLogFailures()>

Checks the errors against known failures.

The $LogInfo structure is augmented with the following fields:
=over

=item MsgGroups
=over

=item Failures
A hashtable mapping error indices to the list of matching known
failure ids.

=back
=back

Returns a hashtable containing a summary of the log failures:
=over

=item LogName
The log file basename.

=item Collection
A collection containing the relevant Failure objects.

=item Failures
A hashtable indexed by the failure ids. Each entry contains:

=over

=item Failure
The failure object.

=item NewCount
A count of errors matched by this known failure that were tagged as new.

=item OldCount
A count of errors matched by this known failure that were tagged as old.

=back

=back
=back
=cut

sub MatchLogFailures($$)
{
  my ($LogInfo, $Task) = @_;

  my $LogFailures = {
    Task => $Task,
    LogName => $LogInfo->{LogName}
  };
  return $LogFailures if (!@{$LogInfo->{MsgGroupNames}});

  my %FailureTree;
  my $ConfigName = $Task->VMName .":$LogInfo->{LogName}";

  $LogFailures->{Collection} = CreateFailures();
  $LogFailures->{Collection}->AddFilter("BugStatus", ["deleted"], "<>");
  foreach my $Failure (@{$LogFailures->{Collection}->GetItems()})
  {
    # Ignore failures that don't apply to this configuration
    my $ConfigRegExp = $Failure->ConfigRegExp;
    my $Match = eval {
      use warnings FATAL => qw(regexp);
      !$ConfigRegExp or $ConfigName =~ /$ConfigRegExp/;
    };
    next if (!$Match);

    my $UnitFailures = $FailureTree{$Failure->ErrorGroup}->{$Failure->TestUnit} ||= [];
    push @$UnitFailures, $Failure;
  }

  foreach my $GroupName (@{$LogInfo->{MsgGroupNames}})
  {
    next if (!$FailureTree{$GroupName});

    my $Group = $LogInfo->{MsgGroups}->{$GroupName};
    foreach my $MsgIndex (0..$#{$Group->{Messages}})
    {
      my $Line = $Group->{Messages}->[$MsgIndex];
      my $TestUnit = $Line =~ /^([_a-z0-9]+)\.c:\d+:[0-9.]* / ? $1 :
                     $Line =~ /^[0-9a-f]+:([_a-z0-9]+):[0-9.]* unhandled exception / ? $1 :
                     "";
      my $UnitFailures = $FailureTree{$GroupName}->{$TestUnit};
      next if (!$UnitFailures);

      # Allow regular expressions written for regular failures to still match
      # when the test has been marked as flaky to ensure a smooth transition.
      my $NonFlakyLine = $Line;
      $NonFlakyLine =~ s/Test succeeded inside flaky todo block:/Test succeeded inside todo block:/g;
      $NonFlakyLine =~ s/Test marked flaky:/Test failed:/g;

      my $AddToKnownNew = $Group->{IsNew}->[$MsgIndex];
      foreach my $UnitFailure (@$UnitFailures)
      {
        my $RegExp = $UnitFailure->FailureRegExp;
        my $Match = eval {
          use warnings FATAL => qw(regexp);
          $RegExp and ($Line =~ /$RegExp/ or $NonFlakyLine =~ /$RegExp/);
        };
        next if (!$Match);

        my $LineFailures = $Group->{Failures}->{$MsgIndex} ||= [];
        push @$LineFailures, $UnitFailure->Id;
        if ($AddToKnownNew)
        {
          my $Type = $Group->{Types}->[$MsgIndex];
          $Group->{"${Type}KnownNew"}++;
          $LogInfo->{"${Type}KnownNew"}++;
          $AddToKnownNew = undef;
        }

        my $LogFailure = $LogFailures->{Failures}->{$UnitFailure->Id};
        if (!$LogFailure)
        {
          $LogFailure = $LogFailures->{Failures}->{$UnitFailure->Id} =
                        { Failure => $UnitFailure };
        }
        my $Count = $Group->{IsNew}->[$MsgIndex] ? "NewCount" : "OldCount";
        $LogFailure->{$Count}++;
      }
    }
  }

  return $LogFailures;
}

=pod
=over 12

=item C<SaveLogFailures()>

Updates the Failures and TaskFailures objects for the task and log specified
by the $LogFailures structure.

Notes:
* This implies deleting any preexisting TaskFailure to avoid leaving obsolete
  data.
* This also updates the failures LastNew and LastOld fields which can however
  not be undone.

=back
=cut

sub SaveLogFailures($)
{
  my ($LogFailures) = @_;

  # FIXME $Task->Failures->AddFilter() cannot be undone and impacts every
  #       future use of $Task->Failures. So add the filter on a throw away
  #       clone to not end up with a nonsensical filter.
  my $TaskFailures = $LogFailures->{Task}->Failures->Clone();
  $TaskFailures->AddFilter("TaskLog", [$LogFailures->{LogName}]);
  my $ErrMessage = $TaskFailures->DeleteAll();
  return $ErrMessage if (defined $ErrMessage);
  return undef if (!$LogFailures->{Failures});

  my $Started = $LogFailures->{Task}->Started;
  foreach my $LogFailure (values %{$LogFailures->{Failures}})
  {
    my $Failure = $LogFailure->{Failure};
    my $TaskFailure = $Failure->TaskFailures->Add();
    my $OldKey = $TaskFailure->GetKey();
    $TaskFailure->Task($LogFailures->{Task});
    $TaskFailure->TaskLog($LogFailures->{LogName});
    $TaskFailure->KeyChanged($OldKey, $TaskFailure->GetKey());
    $TaskFailure->NewCount($LogFailure->{NewCount});
    $TaskFailure->OldCount($LogFailure->{OldCount});

    if ($LogFailure->{NewCount} and ($Failure->LastNew || 0) < $Started)
    {
      $Failure->LastNew($Started);
    }
    if ($LogFailure->{OldCount} and ($Failure->LastOld || 0) < $Started)
    {
      $Failure->LastOld($Started);
    }
  }

  (my $_ErrKey, my $_ErrProperty, $ErrMessage) = $LogFailures->{Collection}->Save();
  return $ErrMessage;
}


#
# Log errors caching [Part 2]
#

sub CreateLogErrorsCache($;$)
{
  my ($LogInfo, $Task) = @_;

  return $LogInfo->{BadLog} if (defined $LogInfo->{BadLog});

  if ($Task and @{$LogInfo->{MsgGroupNames}})
  {
    TagNewMessages($LogInfo, $Task);
    if ($LogInfo->{NoRef} and $LogInfo->{LogName} !~ /\.report$/)
    {
      # In the absence of a reference report, consider all non-test errors
      # to be new, mostly for the benefit of build errors.
      # But don't do so for test errors to avoid false positives.
      MarkAllMessagesAsNew($LogInfo);
    }
  }
  my $LogFailures = MatchLogFailures($LogInfo, $Task) if ($Task);

  my $ErrMessage = _SaveLogErrors($LogInfo);
  $ErrMessage ||= SaveLogFailures($LogFailures) if ($Task);
  return $ErrMessage;
}


#
# Reference report management
#

=pod
=over 12

=item C<SnapshotLatestReport()>

Takes a snapshot of the reference WineTest results for the specified Task.

The reference reports are used to identify new failures, even long after the
task has been run.

Note also that comparing reports in this way may be a bit inaccurate right
after a Wine commit due to delays in getting new WineTest results, etc.
See WineSendLog.pl for more details.

=back
=cut

sub SnapshotLatestReport($$)
{
  my ($Task, $ReportName) = @_;

  my @ErrMessages;
  my $TaskDir = $Task->GetDir();
  my $ReportAge = -M "$TaskDir/$ReportName";

  my $RefReportPaths = $Task->GetRefReportPaths($ReportName, "(?:\.errors)?",
                                                "$DataDir/latest");
  while (my ($RefReportName, $RefReportPath) = each %$RefReportPaths)
  {
    # Ignore reference results more recent than the report
    next if ($ReportAge and -M $RefReportPath <= $ReportAge);

    my @RefNames = ($RefReportName, "$RefReportName.errors");
    if ($ReportName ne "task.log")
    {
      # For non-build tasks, also snapshot the corresponding task.log file
      my $TaskLog = $RefReportName;
      $TaskLog =~ s/-$ReportName$/-task.log/;
      if (-f "$DataDir/latest/$TaskLog")
      {
        push @RefNames, $TaskLog, "$TaskLog.errors";
      }
    }
    foreach my $RefName (@RefNames)
    {
      unlink "$TaskDir/$RefName";
      if (!link("$DataDir/latest/$RefName", "$TaskDir/$RefName"))
      {
        push @ErrMessages, "Could not create the '$RefName' link: $!";
      }
    }
  }

  return \@ErrMessages;
}

sub _IsReportRedundant($$)
{
  my ($RefInfo, $LogInfo) = @_;

  return 1 if (@{$LogInfo->{MsgGroupNames}} == 0);

  foreach my $GroupName (@{$LogInfo->{MsgGroupNames}})
  {
    my $LogGroup = $LogInfo->{MsgGroups}->{$GroupName};
    my $RefGroup = $RefInfo->{MsgGroups}->{$GroupName};
    return undef if (!$RefGroup);

    my $Diff = Algorithm::Diff->new($RefGroup->{Messages}, $LogGroup->{Messages},
                                    { keyGen => \&_GetLineKey });
    while ($Diff->Next())
    {
      # Same() > 0 => Items(2) > 0 despite there being no new lines
      if (!$Diff->Same() and $Diff->Items(2) > 0)
      {
        # This old log has errors not present in the latest one so keep it
        return undef;
      }
    }
  }

  return 1;
}

sub _DeduplicateLatestReport($)
{
  my ($RefReportName) = @_;

  my $LatestGlob = $RefReportName;
  $LatestGlob =~ s/-job[0-9.]+-/-job*-/;
  my @LatestPaths = glob("$DataDir/latest/$LatestGlob");
  return undef if (@LatestPaths <= 1);

  my $RefReportPath = "$DataDir/latest/$RefReportName";
  my $RefInfo = LoadLogErrors($RefReportPath);
  return $RefInfo->{BadLog} if (defined $RefInfo->{BadLog});

  my %LatestAges;
  foreach my $LogPath (@LatestPaths)
  {
    $LatestAges{$LogPath} = -M $LogPath || 0;
  }
  my $RefAge = $LatestAges{$RefReportPath};

  my $ErrMessage;
  my $ReportRE = $RefReportName;
  $ReportRE =~ s/^([a-zA-Z0-9_]+)-job[0-9.]+-([a-zA-Z0-9_.]+)$/$1-job[0-9.]+-$2/;
  foreach my $LogPath (sort { $LatestAges{$a} <=> $LatestAges{$b} } @LatestPaths)
  {
    my $LogName = basename($LogPath);
    next if ($LogName eq $RefReportName);
    next if ($LogName !~ /^($ReportRE)$/);
    $LogName = $1; # untaint
    $LogPath = "$DataDir/latest/$LogName";

    my $LogInfo = LoadLogErrors($LogPath);
    if (defined $LogInfo->{BadLog})
    {
      # Take note of the error but continue to try deduplicating
      $ErrMessage = $LogInfo->{BadLog};
      next;
    }
    if ($RefAge < $LatestAges{$LogPath})
    {
      if (_IsReportRedundant($RefInfo, $LogInfo))
      {
        unlink $LogPath, "$LogPath.errors";
      }
    }
    else
    {
      if (_IsReportRedundant($LogInfo, $RefInfo))
      {
        unlink $RefReportPath, "$RefReportPath.errors";
        last;
      }
    }
  }
  return $ErrMessage;
}

sub UpdateLatestReport($$)
{
  my ($RefReportName, $SrcReportPath) = @_;
  my @ErrMessages;

  foreach my $Suffix ("", ".errors")
  {
    # Add the new reference file even if it is empty.
    next if (!-f "$SrcReportPath$Suffix");

    unlink "$DataDir/latest/$RefReportName$Suffix";
    if (!link("$SrcReportPath$Suffix",
              "$DataDir/latest/$RefReportName$Suffix"))
    {
      push @ErrMessages, "Could not create the '$RefReportName$Suffix' link: $!";
    }
  }

  # The task.log file for a win32 test run is not redundant with one for a
  # wow64 run. So skip deduplication entirely for task.log files.
  if ($RefReportName !~ /-task\.log$/)
  {
    my $ErrMessage = _DeduplicateLatestReport($RefReportName);
    push @ErrMessages, $ErrMessage if (defined $ErrMessage);
  }

  return \@ErrMessages;
}

=pod
=over 12

=item C<UpdateLatestReports()>

Adds the Task's WineTest results to the set of reference reports.

The reference reports will then be used to detect new failures in the other
tasks.

This must be called after SnapshotLatestReport() otherwise a WineTest task
would compare its results to itself.

=back
=cut

sub UpdateLatestReports($$)
{
  my ($Task, $ReportNames) = @_;

  my @ErrMessages;
  my $TaskDir = $Task->GetDir();
  foreach my $ReportName (@$ReportNames)
  {
    next if (!-f "$TaskDir/$ReportName" or -z _);
    my $RefReportName = $Task->GetRefReportName($ReportName);
    push @ErrMessages, @{UpdateLatestReport($RefReportName, "$TaskDir/$ReportName")};
  }
  return \@ErrMessages;
}

1;
