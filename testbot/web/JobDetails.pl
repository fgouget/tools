# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Job details page
#
# Copyright 2009 Ge van Geldorp
# Copyright 2012-2014,2017-2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package StepsTasksBlock;

use ObjectModel::CGI::CollectionBlock;
our @ISA = qw(ObjectModel::CGI::CollectionBlock);

use URI::Escape;

use ObjectModel::CGI::ValueFormatter;

use WineTestBot::Utils;
use WineTestBot::Missions;
use WineTestBot::Engine::Notify;


sub Create($$)
{
  my ($Collection, $EnclosingPage) = @_;

  return StepsTasksBlock->new($Collection, $EnclosingPage);
}

#
# Individual item property support
#

sub DisplayProperty($$)
{
  my ($self, $PropertyDescriptor) = @_;

  my $PropertyName = $PropertyDescriptor->GetName();
  return $PropertyName =~ /^(?:Id|StepNo|PreviousNo|Type|VM|FileType|Missions|NewWarnings|NewTestFailures)$/ ? "" :
         $PropertyName eq "Started" ? ("ro", "timetipdate") :
         $self->SUPER::DisplayProperty($PropertyDescriptor);
}


#
# Item cell generation
#

sub GenerateHeaderView($$$)
{
  my ($self, $Row, $Col) = @_;

  my $PropertyName = $Col->{Descriptor}->GetName();
  if ($PropertyName eq "TaskNo")
  {
    print "<a class='title' title='Step.Task'>Task</a>";
  }
  elsif ($PropertyName eq "CmdLineArg")
  {
    print "Arguments / <span class='MissionHeader'>Missions</span>";
  }
  elsif ($PropertyName eq "Started")
  {
    print "<a class='title' title='Start Date'>Start</a>";
  }
  elsif ($PropertyName eq "Ended")
  {
    print "<a class='title' title='End Time'>Time</a>";
  }
  elsif ($PropertyName eq "Warnings")
  {
    print "<a class='title' title='New / All'>Warnings</a>";
  }
  elsif ($PropertyName eq "TestFailures")
  {
    print "<a class='title' title='New / All'>Failures</a>";
  }
  elsif ($PropertyName eq "FailedTestUnits")
  {
    print "<a class='title' title='Failed test units'>Units</a>";
  }
  else
  {
    $self->SUPER::GenerateHeaderView($Row, $Col);
  }
}

sub GenerateDataView($$$)
{
  my ($self, $Row, $Col) = @_;

  my $StepTask = $Row->{Item};
  my $PropertyName = $Col->{Descriptor}->GetName();
  if ($PropertyName eq "TaskNo")
  {
    print $StepTask->StepNo, ".", $StepTask->TaskNo;
    return;
  }
  if ($PropertyName eq "VMName")
  {
    print "<a href='#k", $StepTask->GetKey(), "'>",
          $self->escapeHTML($StepTask->VMName), "</a>";
    return;
  }
  if ($PropertyName eq "FileName")
  {
    my $FileName = $StepTask->GetFullFileName();
    if ($FileName and -r $FileName)
    {
      my $JobId = $self->{EnclosingPage}->GetJob()->Id;
      my $URI = "/GetFile.pl?JobKey=$JobId&StepKey=". $StepTask->StepNo;
      print "<a href='", $self->escapeHTML($URI), "'>",
            $self->escapeHTML($StepTask->FileName), "</a>";
    }
    return;
  }
  if ($PropertyName eq "CmdLineArg")
  {
    my $Args = $self->escapeHTML($StepTask->CmdLineArg);
    if ($Args eq "" or $StepTask->VM->Type eq "wine")
    {
      $Args .= "<br>" if ($Args ne "");
      my ($ErrMessage, $Missions) = ParseMissionStatement($StepTask->Missions);
      if (defined $ErrMessage)
      {
        $Args .= "<span class='Mission'>$ErrMessage</span>";
      }
      else
      {
        $Args .= "<span class='Mission'>". $self->escapeHTML(GetTaskMissionDescription($Missions->[0], $StepTask->Type)) ."</span>";
      }
    }
    print $Args;
    return;
  }
  if ($PropertyName eq "Ended" and defined $StepTask->Ended)
  {
    my $Duration = $StepTask->Ended - $StepTask->Started;
    GenerateTipDateTime($StepTask->Ended, DurationToString($Duration));
    return;
  }
  if ($PropertyName eq "Warnings" and defined $StepTask->Warnings)
  {
    print "<a href='#k", $StepTask->GetKey(), "'>";
    if (defined $StepTask->NewWarnings)
    {
      my $class = $StepTask->NewWarnings ? "testfail" : "success";
      print "<span class='$class'>", $StepTask->NewWarnings, "</span> / ";
    }
    my $class = $StepTask->Warnings ? "testwarn" : "success";
    print "<span class='$class'>", $StepTask->Warnings, "</span></a>";
    return;
  }
  if ($PropertyName eq "TestFailures" and defined $StepTask->TestFailures)
  {
    print "<a href='#k", $StepTask->GetKey(), "'>";
    if (defined $StepTask->NewTestFailures)
    {
      my $class = $StepTask->NewTestFailures ? "testfail" : "success";
      print "<span class='$class'>", $StepTask->NewTestFailures, "</span> / ";
    }
    my $class = $StepTask->TestFailures ? "testfail" : "success";
    print "<span class='$class'>", $StepTask->TestFailures, "</span></a>";
    return;
  }
  if ($PropertyName eq "FailedTestUnits" and defined $StepTask->FailedTestUnits)
  {
    my $class = $StepTask->TestFailures ? "testfail" : "success";
    print "<a href='#k", $StepTask->GetKey(), "'><span class='$class'>",
          $StepTask->FailedTestUnits, "</span></a>";
    return;
  }
  $self->SUPER::GenerateDataView($Row, $Col);
}


#
# Per-item actions handling
#

sub GetItemActions($)
{
  #my ($self) = @_;
  return [];
}


#
# Actions handling
#

sub CanDoJobControl($$)
{
  my ($self, $Action) = @_;

  my $Job = $self->{EnclosingPage}->GetJob();
  my $Session = $self->{EnclosingPage}->GetCurrentSession();
  my $CurrentUser = $Session->User if (defined $Session);
  if (!$CurrentUser or
      (!$CurrentUser->HasRole("admin") and
       $Job->User->GetKey() ne $CurrentUser->GetKey()))
  {
    return "You are not authorized to $Action this job";
  }

  my %AllowedStatus = (
    "stop" => {"queued" => 1, "running" => 1},
    "resume" => {"canceled" => 1},
    "restart" => {"boterror" => 1, "canceled" => 1},
  );
  return $AllowedStatus{$Action}->{$Job->Status} ? undef :
         "Cannot $Action a ". $Job->Status ." job";
}

sub OnJobControl($$)
{
  my ($self, $Action) = @_;

  return !1 if ($self->{EnclosingPage}->AddError($self->CanDoJobControl($Action)));

  my $JobId = $self->{EnclosingPage}->GetJob()->Id;
  return !1 if ($self->{EnclosingPage}->AddError(JobControl($Action, $JobId)));

  # Ideally this would use something like GetMoreInfoLink() to rebuild a Get
  # URL to preserve which logs and screenshots have been expanded. But if the
  # job got restarted those would be gone anyway.
  # So just do a basic reload to refresh the tasks' status.
  exit($self->{EnclosingPage}->Redirect($ENV{"SCRIPT_NAME"} ."?Key=$JobId"));
}

sub GetActions($)
{
  my ($self) = @_;

  my @Actions;
  push @Actions, "Stop job" if (!$self->CanDoJobControl("stop"));
  push @Actions, "Resume job" if (!$self->CanDoJobControl("resume"));
  push @Actions, "Restart job" if (!$self->CanDoJobControl("restart"));
  return \@Actions;
}

sub OnAction($$)
{
  my ($self, $Action) = @_;

  return $Action eq "Stop job" ? $self->OnJobControl("stop") :
         $Action eq "Resume job" ? $self->OnJobControl("resume") :
         $Action eq "Restart job" ? $self->OnJobControl("restart") :
         $self->SUPER::OnAction($Action);
}


package JobDetailsPage;

use ObjectModel::CGI::CollectionPage;
our @ISA = qw(ObjectModel::CGI::CollectionPage);

use URI::Escape;

use WineTestBot::Config;
use WineTestBot::Failures;
use WineTestBot::Jobs;
use WineTestBot::Log; # For Elapsed()
use WineTestBot::LogUtils;
use WineTestBot::StepsTasks;


sub _initialize($$$)
{
  my ($self, $Request, $RequiredRole) = @_;
  $self->{start} = Time();

  $self->{JobId} = $self->GetParam("Key");
  $self->{JobId} = $self->GetParam("JobId") if (!defined $self->{JobId});

  $self->{Job} = CreateJobs()->GetItem($self->{JobId});
  exit($self->Redirect("/")) if (!defined $self->{Job});

  my $Collection = CreateStepsTasks(undef, $self->{Job});
  $self->SUPER::_initialize($Request, $RequiredRole, $Collection, \&StepsTasksBlock::Create);
  if ($self->{Job}->Status =~ /^(?:queued|running)$/)
  {
    $self->SetRefreshInterval(30);
  }
  $self->{Failures} = CreateFailures();
}

sub GetJob($)
{
  my ($self) = @_;

  return $self->{Job};
}

sub GetPageTitle($)
{
  my ($self) = @_;

  my $PageTitle = $self->{Job}->Remarks;
  $PageTitle =~ s/^[[]\Q$PatchesMailingList\E[]] //;
  $PageTitle = "Job " . $self->{JobId} if ($PageTitle eq "");
  $PageTitle .= " - ${ProjectName} Test Bot";
  return $PageTitle;
}

sub GetTitle($)
{
  my ($self) = @_;

  return "Job $self->{JobId} - ". $self->{Job}->Remarks;
}

sub InitMoreInfo($)
{
  my ($self) = @_;

  my $More = $self->{More} = {};
  my $SortedStepTasks = $self->{Collection}->GetSortedItems();
  foreach my $StepTask (@$SortedStepTasks)
  {
    my $Key = $StepTask->GetKey();
    $More->{$Key}->{Screenshot} = $self->GetParam("s$Key");

    my $Value = $self->GetParam("f$Key");
    my $TaskDir = $StepTask->GetTaskDir();
    foreach my $Log (@{GetLogFileNames($TaskDir, 1)})
    {
      push @{$More->{$Key}->{Logs}}, $Log;
      $More->{$Key}->{Full} = $Log if ($Log eq $Value);
    }
    $More->{$Key}->{Full} ||= "";
  }
}

sub GetMoreInfoLink($$$$;$)
{
  my ($self, $LinkKey, $Label, $Set, $Value) = @_;

  my $Url = $ENV{"SCRIPT_NAME"} ."?Key=$self->{JobId}";

  my $Action = "Show". ($Set eq "Full" and $Label !~ /old/ ? " full" : "");
  foreach my $Key (sort keys %{$self->{More}})
  {
    my $MoreInfo = $self->{More}->{$Key};
    if ($Key eq $LinkKey and $Set eq "Screenshot")
    {
      if (!$MoreInfo->{Screenshot})
      {
        $Url .= "&s$Key=1";
      }
      else
      {
        $Action = "Hide";
      }
    }
    else
    {
      $Url .= "&s$Key=1" if ($MoreInfo->{Screenshot});
    }

    if ($Key eq $LinkKey and $Set eq "Full")
    {
      if ($MoreInfo->{Full} ne $Value)
      {
        $Url .= "&f$Key=". uri_escape($Value);
      }
      else
      {
        $Action = "Hide";
      }
    }
    else
    {
      $Url .= "&f$Key=". uri_escape($MoreInfo->{Full}) if ($MoreInfo->{Full});
    }
  }
  $Url .= "#k" . uri_escape($LinkKey);
  return ($Action, $Url);
}

sub GenerateMoreInfoLink($$$$;$$)
{
  my ($self, $LinkKey, $Label, $Set, $Value, $StepTask) = @_;

  my ($Action, $Url) = $self->GetMoreInfoLink($LinkKey, $Label, $Set, $Value);
  my $Title = ($Value =~ /^(.*)\.report$/) ? " title='$1'" : "";

  my $Html = "<a href='". $self->escapeHTML($Url) ."'$Title>$Action $Label</a>";
  if (defined $Value)
  {
    $Url = "/GetTaskFile.pl?Job=$self->{JobId}"
           ."&Step=". $StepTask->StepNo
           ."&Task=". $StepTask->TaskNo
           ."&File=". uri_escape($Value);
    $Html = "<a href='". $self->escapeHTML($Url) ."'>&darr;</a> $Html";
  }
  if ($Action eq "Hide")
  {
    $Html = "<span class='TaskMoreInfoSelected'>$Html</span>";
  }
  print "<div class='TaskMoreInfoLink'>$Html</div>\n";
}

sub GetFailureLink($$;$)
{
  my ($self, $Id, $Str) = @_;

  my $Failure = $self->{Failures}->GetItem($Id);
  $Str ||= $Failure ? $Failure->BugId : "F$Id";
  my $Desc = $Failure->BugDescription if ($Failure);
  $Desc = ": ". $self->escapeHTML($Desc) if ($Desc);
  return "<a href='FailureDetails.pl?Key=$Id' title='Failure $Id$Desc'>$Str</a>";
}

sub GetFailureLinks($$)
{
  my ($self, $FailureIds) = @_;

  return join(" ", map { $self->GetFailureLink($_) }
                   sort {
                     my $FA = $self->{Failures}->GetItem($a);
                     my $FB = $self->{Failures}->GetItem($b);
                     return ($FA ? $FA->BugId : 0) <=> ($FB ? $FB->BugId : 0)
                            || $a <=> $b;
                   } @$FailureIds);
}

sub GenerateFullLog($$$$$)
{
  my ($self, $Dir, $LogId, $LogName, $HideLog) = @_;

  my $LogInfo = LoadLogErrors("$Dir/$LogName");
  if (defined $LogInfo->{BadLog})
  {
    print "<pre class='log-note'>Could not highlight new errors: ", ($LogInfo->{BadRef} || $LogInfo->{BadLog}), "</pre>\n";
  }
  elsif ($LogInfo->{NoRef} and !defined $LogInfo->{BadRef})
  {
    print "<pre class='log-note'>No WineTest results are available to detect new errors</pre>\n";
  }
  elsif ($LogInfo->{NoRef})
  {
    print "<pre class='log-note'>Could not detect new errors: $LogInfo->{BadRef}</pre>\n";
  }
  elsif (defined $LogInfo->{BadRef})
  {
    print "<pre class='log-note'>Some WineTest results could not be used to detect new errors: $LogInfo->{BadRef}</pre>\n";
  }

  my (%ErrCategory, %ErrFailures, %LogBugs);
  foreach my $GroupName (@{$LogInfo->{MsgGroupNames}})
  {
    my $Group = $LogInfo->{MsgGroups}->{$GroupName};
    for my $MsgIndex (0..$#{$Group->{Messages}})
    {
      my $LineNo = $Group->{LineNos}->[$MsgIndex];
      if ($LineNo)
      {
        $ErrCategory{$LineNo} = ($Group->{IsNew}->[$MsgIndex] ? "new" : "").
                                ($Group->{Types}->[$MsgIndex] eq "k" ? "flaky" :
                                 $Group->{Types}->[$MsgIndex] eq "w" ? "warn" :
                                 "error");
        $ErrFailures{$LineNo} = $Group->{Failures}->{$MsgIndex};

        if ($ErrFailures{$LineNo})
        {
          foreach my $FailureId (@{$ErrFailures{$LineNo}})
          {
           my $Failure = $self->{Failures}->GetItem($FailureId);
           $LogBugs{$Failure->BugId} ||= [$LineNo, $Failure->BugDescription];
          }
        }
      }
      $LogBugs{-2} ||= [$LineNo, "First failure"];
      if ($ErrCategory{$LineNo} eq "newerror" and !$ErrFailures{$LineNo})
      {
        $LogBugs{-1} ||= [$LineNo, "First untracked new failure"];
        $LogBugs{10000000} = [$LineNo, "Last untracked new failure"];
      }
      $LogBugs{10000001} = [$LineNo, "Last failure"];
    }
  }
  if (%LogBugs)
  {
    print "<p>Failures index:";
    foreach my $Label (sort { $a <=> $b } keys %LogBugs)
    {
      my ($LineNo, $Title) = @{$LogBugs{$Label}};
      $Label = $Label == -2 ? "First" :
               $Label == -1 ? "new" :
               $Label == 10000000 ? "new" :
               $Label == 10000001 ? "Last" :
               $Label;
      $Title = " title='". $self->escapeHTML($Title) ."'" if ($Title);
      print " <a href='#k${LogId}L$LineNo'$Title>$Label</a>"
    }
    print "</p>\n";
  }

  my $GetCategory = $LogName =~ /\.report$/ ? \&GetReportLineCategory :
                    \&GetLogLineCategory;

  my $LineNo = 0;
  my $IsEmpty = 1;
  my $LineNo = 0;
  if (open(my $LogFile, "<", "$Dir/$LogName"))
  {
    foreach my $Line (<$LogFile>)
    {
      $LineNo++;
      $Line =~ s/\s*$//;
      if ($IsEmpty)
      {
        print "<pre$HideLog><code>";
        $IsEmpty = 0;
      }

      my $Html = $self->escapeHTML($Line);
      my $Category = $ErrCategory{$LineNo} || $GetCategory->($Line);
      if ($Category ne "none")
      {
        $Html =~ s~^(.*\S)\s*\r?$~<span id='k${LogId}L$LineNo' class='log-$Category'>$1</span>~;
      }
      if ($ErrFailures{$LineNo})
      {
        print "[", $self->GetFailureLinks($ErrFailures{$LineNo}), "] ";
      }
      print "$Html\n";
    }
    close($LogFile);
  }
  else
  {
    print "<pre class='log-error'><code>Unable to open '$LogName' for reading: $!</code></pre>\n";
    $IsEmpty = 0;
  }

  # And append the extra errors
  foreach my $GroupName (@{$LogInfo->{MsgGroupNames}})
  {
    my $Group = $LogInfo->{MsgGroups}->{$GroupName};
    # Extra groups don't have a line number
    next if ($Group->{LineNo});

    # Extra errors don't have line numbers but there should be few of them.
    # So just create one id for them all so the shortcut  to the first / last
    # error has somewhere to go even if it's not 100% accurate.
    print "<div id='k${LogId}L0' class='LogDllName'>$GroupName</div>\n";
    for my $MsgIndex (0..$#{$Group->{Messages}})
    {
      if ($IsEmpty)
      {
        print "<pre$HideLog><code>";
        $IsEmpty = 0;
      }

      my $Line = $Group->{Messages}->[$MsgIndex];
      my $Category = $Group->{IsNew}->[$MsgIndex] ? "newerror" : "error";
      my $Html = $self->escapeHTML($Line);
      if ($Group->{Failures}->{$MsgIndex})
      {
        print "[", $self->GetFailureLinks($Group->{Failures}->{$MsgIndex}), "] ";
      }
      print "<span class='log-$Category'>$Html</span>\n";
    }
  }

  print "</code></pre>\n" if (!$IsEmpty);
  return $IsEmpty;
}

sub GenerateBody($)
{
  my ($self) = @_;

  $self->SUPER::GenerateBody();

  $self->InitMoreInfo();

  print <<EOF;
<script type='text/javascript'>
<!--
function HideLog(event, url)
{
  // Ignore double-clicks on the log text (i.e. on the <code> element) to
  // allow word-selection
  if (event.target.nodeName == 'PRE' && !event.altKey && !event.ctrlKey &&
      !event.metaKey && !event.shiftKey)
  {
    window.open(url, "_self", "", true);
  }
}
//-->
</script>
EOF

  print "<div class='Content'>\n";
  my $Index = 0;
  my $SortedStepTasks = $self->{Collection}->GetSortedItems();
  foreach my $StepTask (@$SortedStepTasks)
  {
    my $Key = $StepTask->GetKey();
    my $TaskDir = $StepTask->GetTaskDir();
    my $VM = $StepTask->VM;

    my $Prev = $Index > 0 ? "k". $SortedStepTasks->[$Index-1]->GetKey() : "PageTitle";
    my $Next = $Index + 1 < @$SortedStepTasks ? "k". $SortedStepTasks->[$Index+1]->GetKey() : "PageEnd";
    $Index++;
    print "<h2><a name='k", $self->escapeHTML($Key), "'></a>",
          $self->escapeHTML($StepTask->GetTitle()),
          " <span class='right'><a class='title' href='#$Prev'>&uarr;</a><a class='title' href='#$Next'>&darr;</a></span></h2>\n";

    print "<details><summary>",
          $self->escapeHTML($VM->Description || $VM->Name), "</summary>",
          $self->escapeHTML($VM->Details || "No details!"),
          ($StepTask->Missions ? "<br>Missions: ". $StepTask->Missions : ""),
          "</details>\n";

    my $MoreInfo = $self->{More}->{$Key};
    print "<div class='TaskMoreInfoLinks'>\n";
    if (-r "$TaskDir/screenshot.png")
    {
      if ($MoreInfo->{Screenshot})
      {
        my $URI = "/Screenshot.pl?JobKey=$self->{JobId}"
                  ."&StepKey=". $StepTask->StepNo
                  ."&TaskKey=". $StepTask->TaskNo;
        print "<div class='Screenshot'><img src='", $self->escapeHTML($URI),
              "' alt='Screenshot'/></div>\n";
      }
      $self->GenerateMoreInfoLink($Key, "final screenshot", "Screenshot");
    }

    my $ReportCount;
    foreach my $LogName (@{$MoreInfo->{Logs}})
    {
      $self->GenerateMoreInfoLink($Key, GetLogLabel($LogName), "Full", $LogName, $StepTask);
      $ReportCount++ if ($LogName !~ /^old_/ and $LogName =~ /\.report$/);
    }
    print "</div>\n";

    if ($MoreInfo->{Full})
    {
      #
      # Show this log in full, highlighting the important lines
      #

      my ($Action, $Url) = $self->GetMoreInfoLink($Key, GetLogLabel($MoreInfo->{Full}), "Full", $MoreInfo->{Full});
      $Url = $self->escapeHTML($Url);
      my $HideLog = $Action eq "Hide" ? " ondblclick='HideLog(event, \"$Url\")'" : "";

      my $LogIsEmpty = $self->GenerateFullLog($TaskDir, $StepTask->GetKey(), $MoreInfo->{Full}, $HideLog);
      if ($LogIsEmpty)
      {
        if ($StepTask->Status eq "canceled")
        {
          print "No log, task was canceled\n";
        }
        elsif ($StepTask->Status eq "skipped")
        {
          print "No log, task skipped\n";
        }
        else
        {
          print "Empty log\n";
        }
      }
    }
    else
    {
      #
      # Show a summary of the errors from all the reports and logs
      #

      # Figure out which logs / reports actually have errors
      my $LogInfos;
      foreach my $LogName (@{$MoreInfo->{Logs}})
      {
        next if ($LogName =~ /^old_/);
        my $LogInfo = LoadLogErrors("$TaskDir/$LogName");
        if (defined $LogInfo->{BadLog} or @{$LogInfo->{MsgGroupNames}})
        {
          $LogInfos->{$LogName} = $LogInfo;
        }
      }
      my $ShowLogName = ($ReportCount > 1 or scalar(keys %$LogInfos) > 1);

      my $NoErrors = 1;
      foreach my $LogName (@{$MoreInfo->{Logs}})
      {
        next if (!$LogInfos->{$LogName});
        $NoErrors = 0;

        if ($ShowLogName)
        {
          # Show the log / report name to avoid ambiguity
          my $Label = ucfirst(GetLogLabel($LogName));
          print "<div class='HrTitle'>$Label<div class='HrLine'></div></div>\n";
        }

        my $LogInfo = $LogInfos->{$LogName};
        if (defined $LogInfo->{BadLog})
        {
          my ($_Action, $Url) = $self->GetMoreInfoLink($Key, GetLogLabel($LogName), "Full", $LogName);
          print "<pre class='log-note'>The error summary is not available (<a href='", $self->escapeHTML($Url), "'>see full log instead</a>): $LogInfo->{BadLog}</pre>\n";
        }
        elsif ($LogInfo->{NoRef} and !defined $LogInfo->{BadRef})
        {
          print "<pre class='log-note'>No WineTest results are available to detect new errors</pre>\n";
        }
        elsif ($LogInfo->{NoRef})
        {
          print "<pre class='log-note'>Could not detect new errors: $LogInfo->{BadRef}</pre>\n";
        }
        elsif (defined $LogInfo->{BadRef})
        {
          print "<pre class='log-note'>Some WineTest results could not be used to detect new errors: $LogInfo->{BadRef}</pre>\n";
        }

        foreach my $GroupName (@{$LogInfo->{MsgGroupNames}})
        {
          print "<div class='LogDllName'>$GroupName</div>\n" if ($GroupName);

          print "<pre><code>";
          my $MsgIndex = 0;
          my $Group = $LogInfo->{MsgGroups}->{$GroupName};
          foreach my $Line (@{$Group->{Messages}})
          {
            my $Html = $self->escapeHTML($Line);
            my $Category = ($Group->{IsNew}->[$MsgIndex] ? "new" : "").
                           ($Group->{Types}->[$MsgIndex] eq "k" ? "flaky" :
                            $Group->{Types}->[$MsgIndex] eq "w" ? "warn" :
                            "error");
            $Html = "<span class='log-$Category'>$Html</span>";
            my $FailureIds = $Group->{Failures}->{$MsgIndex};
            if ($FailureIds)
            {
              print "[", $self->GetFailureLinks($FailureIds), "] ";
            }
            print "$Html\n";
            $MsgIndex++;
          }
          print "</code></pre>\n";
        }
      }
      if (!@{$MoreInfo->{Logs}})
      {
        # There is no log file or they are all empty
        if ($StepTask->Status eq "canceled")
        {
          print "No log, task was canceled\n";
        }
        elsif ($StepTask->Status eq "skipped")
        {
          print "No log, task skipped\n";
        }
        else
        {
          print "<pre class='log-note'>No result yet...</pre>\n";
        }
      }
      elsif ($NoErrors)
      {
        print "No errors\n";
      }
    }
  }
  print "</div>\n";
}

sub GenerateFooter($)
{
  my ($self) = @_;
  print "<p class='GeneralFooterText'>Generated in ", Elapsed($self->{start}), " s</p>\n";
}


package main;

my $Request = shift;
my $Page = JobDetailsPage->new($Request, "");
$Page->GeneratePage();
