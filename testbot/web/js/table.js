/* Copyright 2022 Francois Gouget
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
"use strict";

var mcb_count;
var mcb_checked;

function SetCheckboxes(e)
{
    const master = e.target.closest("input");
    const group = master.getAttribute("mcbgroup");
    const selector = 'input[cbgroup="' + group + '"]';
    document.querySelectorAll(selector).forEach(cb => {
        cb.checked = master.checked;
    });
    mcb_checked[group] = master.checked ? mcb_count[group] : 0;
}

function UpdateMasterCb(e)
{
    const cb = e.target.closest("input");
    const group = cb.getAttribute("cbgroup");
    mcb_checked[group] += cb.checked ? 1 : -1;

    const master = document.querySelector('input[mcbgroup="' + group + '"]');
    master.checked = (mcb_count[group] == mcb_checked[group]);
}


function InitMasterCbs()
{
    mcb_count = {};
    mcb_checked = {};
    document.querySelectorAll("input[mcbgroup]").forEach(master => {
        const group = master.getAttribute("mcbgroup");
        mcb_count[group] = mcb_checked[group] = 0;
        const selector = 'input[cbgroup="' + group + '"]';
        document.querySelectorAll(selector).forEach(cb => {
            mcb_count[group]++;
            if (cb.checked) mcb_checked[group]++;
            cb.addEventListener('click', UpdateMasterCb);
        });
        master.checked = (mcb_count[group] == mcb_checked[group]);
        master.addEventListener('click', SetCheckboxes);
        master.hidden = false;
    });
}

window.addEventListener('load', InitMasterCbs);
