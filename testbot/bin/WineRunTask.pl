#!/usr/bin/perl -Tw
# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
#
# Sends and runs the tasks in the Windows test VMs.
#
# Copyright 2009 Ge van Geldorp
# Copyright 2013-2019 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

sub BEGIN
{
  if ($0 !~ m=^/=)
  {
    # Turn $0 into an absolute path so it can safely be used in @INC
    require Cwd;
    $0 = Cwd::cwd() . "/$0";
  }
  if ($0 =~ m=^(/.*)/[^/]+/[^/]+$=)
  {
    $::RootDir = $1;
    unshift @INC, "$::RootDir/lib";
  }
}
my $Name0 = $0;
$Name0 =~ s+^.*/++;


use WineTestBot::Config;
use WineTestBot::Engine::Notify;
use WineTestBot::Jobs;
use WineTestBot::Log;
use WineTestBot::LogUtils;
use WineTestBot::Utils;
use WineTestBot::VMs;


#
# Logging and error handling helpers
#

my $Debug;
sub Debug(@)
{
  print STDERR @_ if ($Debug);
}

my $LogOnly;
sub Error(@)
{
  print STDERR "$Name0:error: ", @_ if (!$LogOnly);
  LogMsg @_;
}


#
# Task helpers
#

sub TakeScreenshot($$)
{
  my ($VM, $FileName) = @_;

  my $Domain = $VM->GetDomain();
  my ($ErrMessage, $ImageSize, $ImageBytes) = $Domain->CaptureScreenImage();
  if (!defined $ErrMessage)
  {
    my $OldUMask = umask(002);
    if (open(my $Screenshot, ">", $FileName))
    {
      print $Screenshot $ImageBytes;
      close($Screenshot);
    }
    else
    {
      Error "Could not open the screenshot file for writing: $!\n";
    }
    umask($OldUMask);
  }
  elsif ($Domain->IsPoweredOn())
  {
    Error "Could not capture a screenshot: $ErrMessage\n";
  }
}


#
# Setup and command line processing
#

my $Usage;
sub ValidateNumber($$)
{
  my ($Name, $Value) = @_;

  # Validate and untaint the value
  return $1 if ($Value =~ /^(\d+)$/);
  Error "$Value is not a valid $Name\n";
  $Usage = 2;
  return undef;
}

my ($JobId, $StepNo, $TaskNo);
while (@ARGV)
{
  my $Arg = shift @ARGV;
  if ($Arg eq "--debug")
  {
    $Debug = 1;
  }
  elsif ($Arg eq "--log-only")
  {
    $LogOnly = 1;
  }
  elsif ($Arg =~ /^(?:-\?|-h|--help)$/)
  {
    $Usage = 0;
    last;
  }
  elsif ($Arg =~ /^-/)
  {
    Error "unknown option '$Arg'\n";
    $Usage = 2;
    last;
  }
  elsif (!defined $JobId)
  {
    $JobId = ValidateNumber('job id', $Arg);
  }
  elsif (!defined $StepNo)
  {
    $StepNo = ValidateNumber('step number', $Arg);
  }
  elsif (!defined $TaskNo)
  {
    $TaskNo = ValidateNumber('task number', $Arg);
  }
  else
  {
    Error "unexpected argument '$Arg'\n";
    $Usage = 2;
    last;
  }
}

# Check parameters
if (!defined $Usage)
{
  if (!defined $JobId || !defined $StepNo || !defined $TaskNo)
  {
    Error "you must specify the job id, step number and task number\n";
    $Usage = 2;
  }
}
if (defined $Usage)
{
    print "Usage: $Name0 [--debug] [--log-only] [--help] JobId StepNo TaskNo\n";
    exit $Usage;
}

my $Job = CreateJobs()->GetItem($JobId);
if (!defined $Job)
{
  Error "Job $JobId does not exist\n";
  exit 1;
}
my $Step = $Job->Steps->GetItem($StepNo);
if (!defined $Step)
{
  Error "Step $StepNo of job $JobId does not exist\n";
  exit 1;
}
my $Task = $Step->Tasks->GetItem($TaskNo);
if (!defined $Task)
{
  Error "Step $StepNo task $TaskNo of job $JobId does not exist\n";
  exit 1;
}
my $OldUMask = umask(002);
my $TaskDir = $Task->CreateDir();
umask($OldUMask);
my $VM = $Task->VM;


my $Start = Time();
LogMsg "Task $JobId/$StepNo/$TaskNo started on ", $VM->Name, "\n";


#
# Error handling helpers
#

sub LogTaskInfo($)
{
  my ($ErrMessage) = @_;
  print STDERR "$Name0:info: ", $ErrMessage;
}

sub LogTaskError($)
{
  my ($ErrMessage) = @_;
  print STDERR "$Name0:error: ", $ErrMessage;
}

my $ReportNames;

sub WrapUpAndExit($;$$$$$$$$)
{
  my ($Status, $TestFailures, $Retry, $TimedOut, $Reason, $NewTestFailures, $NewWarnings, $Warnings, $FailedTestUnits) = @_;
  my $NewVMStatus = $Status eq 'queued' ? 'offline' : 'dirty';
  my $VMResult = $Reason ? $Reason :
                 $Status eq "boterror" ? "boterror" :
                 $Status eq "queued" ? "error" :
                 $TimedOut ? "timeout" : "";

  Debug(Elapsed($Start), " Taking a screenshot\n");
  TakeScreenshot($VM, "$TaskDir/screenshot.png");

  my $Tries = $Task->TestFailures || 0;
  if ($Retry)
  {
    # This may be a transient error (e.g. a network glitch)
    # so retry a few times to improve robustness
    $Tries++;
    if ($Task->CanRetry())
    {
      $Status = 'queued';
      $TestFailures = $Tries;
    }
    else
    {
      LogTaskError("Giving up after $Tries run(s)\n");
    }
  }
  elsif ($Tries >= 1)
  {
    LogTaskInfo("The previous $Tries run(s) terminated abnormally\n");
  }

  # Record result details that may be lost or overwritten by a later run
  if ($VMResult)
  {
    $VMResult .= " $Tries $MaxTaskTries" if ($Retry);
    $VM->RecordResult(undef, $VMResult);
  }

  # Update the Task and Job
  $Task->Status($Status);
  $Task->NewWarnings($NewWarnings);
  $Task->Warnings($Warnings);
  $Task->NewTestFailures($NewTestFailures);
  $Task->TestFailures($TestFailures);
  $Task->FailedTestUnits($FailedTestUnits);
  if ($Status eq 'queued')
  {
    $Task->Started(undef);
    $Task->Ended(undef);
    # Leave the Task files around so they can be seen until the next run
  }
  else
  {
    $Task->Ended(time());
  }
  $Task->Save();
  $Job->UpdateStatus();

  # Get the up-to-date VM status and update it if nobody else changed it
  $VM = CreateVMs()->GetItem($VM->GetKey());
  if ($VM->Status eq 'running')
  {
    $VM->Status($NewVMStatus);
    if ($NewVMStatus eq 'offline')
    {
      $VM->AddError();
    }
    else
    {
      $VM->Errors(undef);
    }
    $VM->ChildDeadline(undef);
    $VM->ChildPid(undef);
    $VM->Save();
  }

  # Update the 'latest/' reference WineTest results
  # Note that if the WineTest run timed out the missing test results would
  # cause false positives. This can only be compensated by having a complete
  # reference report from another run. But if only incomplete reports are
  # available there would still be false positives. So ignore the report in
  # case of a timeout.
  if ($Step->Type eq 'suite' and $Status eq 'completed' and !$TimedOut)
  {
    my $ErrMessages = UpdateLatestReports($Task, $ReportNames);
    Error("$_\n") for (@$ErrMessages);
  }

  my $Result = $VM->Name .": ". $VM->Status ." Status: $Status Failures: ". (defined $TestFailures ? $TestFailures : "unset");
  LogMsg "Task $JobId/$StepNo/$TaskNo done ($Result)\n";
  Debug(Elapsed($Start), " Done. $Result\n");
  exit($Status eq 'completed' ? 0 : 1);
}

# Only to be used if the error cannot be fixed by re-running the task.
# The TestBot will be indicated as having caused the failure.
sub FatalError($;$)
{
  my ($ErrMessage, $Retry) = @_;

  LogMsg "$JobId/$StepNo/$TaskNo $ErrMessage";
  LogTaskError("BotError: $ErrMessage");

  WrapUpAndExit('boterror', undef, $Retry);
}

sub FatalTAError($$;$)
{
  my ($TA, $ErrMessage, $PossibleCrash) = @_;
  $ErrMessage .= ": ". $TA->GetLastError() if (defined $TA);

  # A TestAgent operation failed, see if the VM is still accessible
  my $IsPoweredOn = $VM->GetDomain()->IsPoweredOn();
  if (!defined $IsPoweredOn)
  {
    # The VM host is not accessible anymore so put the VM offline and
    # requeue the task. This does not count towards the task's tries limit
    # since neither the VM nor the task are at fault.
    Error("$ErrMessage\n");
    NotifyAdministrator("Putting the ". $VM->Name ." VM offline",
                        "A TestAgent operation to the ". $VM->Name ." VM failed:\n".
                        "\n$ErrMessage\n".
                        "So the VM has been put offline and the TestBot will try to regain access to it.");
    WrapUpAndExit('queued');
  }

  my $Retry;
  if ($IsPoweredOn)
  {
    LogMsg("$ErrMessage\n");
    LogTaskError("$ErrMessage\n");
    $ErrMessage = "The test VM has crashed, rebooted or lost connectivity (or the TestAgent server died)\n";
    # Retry in case it was a temporary network glitch
    $Retry = 1;
  }
  else
  {
    # Ignore the TestAgent error, it's irrelevant
    $ErrMessage = "The test VM is powered off! Did the test shut it down?\n";
  }
  if ($PossibleCrash and !$Task->CanRetry())
  {
    # The test did it!
    LogTaskError($ErrMessage);
    WrapUpAndExit('completed', 1);
  }
  FatalError($ErrMessage, $Retry);
}


#
# Check the VM and Step
#

if ($VM->Type ne "win32" and $VM->Type ne "win64")
{
  FatalError("This is not a Windows VM! (" . $VM->Type . ")\n");
}
if (!$Debug and $VM->Status ne "running")
{
  # Maybe the administrator tinkered with the VM state? In any case the VM
  # is not ours to use so requeue the task and abort. Note that the VM will
  # not be put offline (again, not ours).
  Error("The VM is not ready for use (" . $VM->Status . ")\n");
  WrapUpAndExit('queued');
}
my $Domain = $VM->GetDomain();
if (!$Domain->IsPoweredOn())
{
  # Maybe the VM was prepared in advance and got taken down by a power outage?
  # Requeue the task and treat this event as a failed revert to avoid infinite
  # loops.
  Error("The VM is not powered on\n");
  NotifyAdministrator("Putting the ". $VM->Name ." VM offline",
    "The ". $VM->Name ." VM should have been powered on to run the task\n".
    "below but its state was ". $Domain->GetStateDescription() ." instead.\n".
    MakeOfficialURL(GetTaskURL($JobId, $StepNo, $TaskNo)) ."\n\n".
    "So the VM has been put offline and the TestBot will try to regain\n".
    "access to it.");
  WrapUpAndExit('queued', undef, undef, undef, 'boterror vm off');
}

if ($Step->Type ne "single" and $Step->Type ne "suite")
{
  FatalError("Unexpected step type '". $Step->Type ."' found\n");
}
if ($Step->FileType ne "exe32" and $Step->FileType ne "exe64")
{
  FatalError("Unexpected file type '". $Step->FileType ."' found for ". $Step->Type ." step\n");
}

(my $ReportErr, $ReportNames, my $TaskMissions) = $Task->GetReportNames();
FatalError "$ReportErr\n" if (defined $ReportErr);
FatalError "Cannot specify multiple missions\n" if (@{$TaskMissions->{Missions}} > 1);
my $Mission = $TaskMissions->{Missions}->[0];
my $RptFileName = $ReportNames->[0];


#
# Setup the VM
#

my $TA = $VM->GetAgent();
my $FileName = $Step->FileName;
Debug(Elapsed($Start), " Sending '". $Step->GetFullFileName() ."'\n");
if (!$TA->SendFile($Step->GetFullFileName(), $FileName, 0))
{
  FatalTAError($TA, "Could not copy the test executable to the VM");
}

my $Keepalive;
my $Timeout = $Task->Timeout;
my $Script = "set WINETEST_DEBUG=" . $Step->DebugLevel .
             "\r\n";
if ($Step->LogTime)
{
  $Script .= "set WINETEST_TIME=1\r\n";
}
if ($Step->ReportSuccessfulTests)
{
  $Script .= "set WINETEST_REPORT_SUCCESS=1\r\n";
}
my $IsWineTest = 1;
if ($Step->Type eq "single")
{
  my $TestLauncher = "TestLauncher" . ($Step->FileType eq "exe64" ? "64" : "32") . ".exe";
  Debug(Elapsed($Start), " Sending 'latest/$TestLauncher'\n");
  if (!$TA->SendFile("$DataDir/latest/$TestLauncher", $TestLauncher, 0))
  {
    FatalTAError($TA, "Could not copy TestLauncher to the VM");
  }

  $Script .= "$TestLauncher -t $Timeout $FileName ";
  # Add some margin so $TA->Wait() does not time out right before
  # TestLauncher does.
  $Timeout += 5;
  $Keepalive = 20;
  my $CmdLineArg = $Task->CmdLineArg;
  if ($CmdLineArg)
  {
    $Script .= "$CmdLineArg ";
  }
  $Script .= "> $RptFileName\r\n";

  # If the user gave us an executable there is no telling
  # if it is going going to follow the Wine test standards.
  $IsWineTest = (defined $Step->PreviousNo and
                 $Job->Steps->GetItem($Step->PreviousNo)->FileType eq "patch");
}
elsif ($Step->Type eq "suite")
{
  $Keepalive = 60;
  $Script .= "$FileName ";
  if (defined($WebHostName))
  {
    my $URL = GetTaskURL($JobId, $StepNo, $TaskNo, 1);
    $Script .= "-u ". BatchQuote(MakeOfficialURL($URL)) ." ";
  }
  my $Tag = $VM->Type ne "win64" ? "" : $Step->FileType eq "exe64" ? "64" : "32";
  $Tag = BuildTag($VM->Name, $Tag);
  my $Info = $VM->Description ? $VM->Description : "";
  if ($VM->Details)
  {
      $Info .= ": " if ($Info ne "");
      $Info .=  $VM->Details;
  }
  # Escape the arguments for cmd's command interpreter
  $Script .= "-q -d tests -o $RptFileName -t $Tag -m ". BatchQuote($AdminEMail) ." -i ". BatchQuote($Info) ."\r\n";
  $Script .= "$FileName -q -s $RptFileName\r\n" if (!$Mission->{nosubmit});
}
Debug(Elapsed($Start), " Sending the script: [$Script]\n");
if (!$TA->SendFileFromString($Script, "script.bat", $TestAgent::SENDFILE_EXE))
{
  FatalTAError($TA, "Could not send the task script to the VM");
}

my $Logger = "script.bat >Task.log 2>&1\r\n";
if (!$TA->SendFileFromString($Logger, "cmdlogger.bat", $TestAgent::SENDFILE_EXE))
{
  FatalTAError($TA, "Could not send the logging script to the VM");
}


#
# Run the test
#

Debug(Elapsed($Start), " Starting the script\n");
my $Pid = $TA->Run(["./cmdlogger.bat"], 0);
if (!$Pid)
{
  FatalTAError($TA, "Failed to start the test");
}


#
# Snapshot the reference reports
#

my $ErrMessages = SnapshotLatestReport($Task, $RptFileName);
LogTaskError("$_\n") for (@$ErrMessages);


#
# From that point on we want to at least try to grab the test
# log and a screenshot before giving up
#

my $NewStatus = 'completed';
my ($TaskFailures, $TaskTimedOut, $TAError, $PossibleCrash);
my ($NewTestFailures, $NewWarnings, $Warnings, $FailedTestUnits);
Debug(Elapsed($Start), " Waiting for the script (", $Timeout, "s timeout)\n");
if (!defined $TA->Wait($Pid, $Timeout, $Keepalive))
{
  my $ErrMessage = $TA->GetLastError();
  if ($ErrMessage =~ /timed out waiting for the child process/)
  {
    LogTaskError("The task timed out\n");
    $TaskFailures = 1;
    $TaskTimedOut = 1;
  }
  else
  {
    $PossibleCrash = 1;
    $TAError = "An error occurred while waiting for the test to complete: $ErrMessage";
  }
}

Debug(Elapsed($Start), " Retrieving 'Task.log'\n");
if ($TA->GetFile("Task.log", "$TaskDir/task.log"))
{
  my $LogInfo = ParseTaskLog("$TaskDir/task.log");
  $TaskFailures ||= $LogInfo->{eCount};
  my $LogErrMsg = CreateLogErrorsCache($LogInfo);
  LogTaskError("$LogErrMsg\n") if (defined $LogErrMsg);
}
elsif (!defined $TAError)
{
  $TAError = "An error occurred while retrieving the task log: ". $TA->GetLastError();
}

Debug(Elapsed($Start), " Retrieving the report file to '$RptFileName'\n");
if ($TA->GetFile($RptFileName, "$TaskDir/$RptFileName"))
{
  chmod 0664, "$TaskDir/$RptFileName";

  my $LogInfo = ParseWineTestReport("$TaskDir/$RptFileName", $IsWineTest, $TaskTimedOut);
  $TaskTimedOut = 1 if ($LogInfo->{TestUnitCount} == $LogInfo->{TimeoutCount});
  if (defined $LogInfo->{BadLog})
  {
    # Could not open the file
    $NewStatus = 'boterror';
    Error "$LogInfo->{BadLog}\n";
    LogTaskError("$LogInfo->{BadLog}\n");
  }
  else
  {
    my $LogErrMsg = CreateLogErrorsCache($LogInfo, $Task);
    LogTaskError("$LogErrMsg\n") if (defined $LogErrMsg);

    # The message counters can legitimately be undefined in case of a timeout
    $TaskFailures += ($LogInfo->{eCount} || 0) + ($LogInfo->{kCount} || 0);
    if ($TaskFailures)
    {
      # Known failures can be tagged as new too but don't count.
      $NewTestFailures = ($LogInfo->{eNew} || 0) - ($LogInfo->{eKnownNew} || 0);
    }
    $Warnings = $LogInfo->{wCount};
    if ($Warnings)
    {
      $NewWarnings = ($LogInfo->{wNew} || 0) - ($LogInfo->{wKnownNew} || 0);
    }
    map { $FailedTestUnits->{$_} = 1 } @{$LogInfo->{FailedTestUnits}};
  }
}
elsif (!defined $TAError)
{
  $TAError = "An error occurred while retrieving $RptFileName: ". $TA->GetLastError();
}

Debug(Elapsed($Start), " Disconnecting\n");
$TA->Disconnect();


#
# Wrap up
#

FatalTAError(undef, $TAError, $PossibleCrash) if (defined $TAError);

WrapUpAndExit($NewStatus, $TaskFailures, undef, $TaskTimedOut, undef, $NewTestFailures, $NewWarnings, $Warnings, $FailedTestUnits ? scalar(%$FailedTestUnits) : undef);
